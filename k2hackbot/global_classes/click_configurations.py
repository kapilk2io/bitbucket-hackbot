import copy
import json
import os
import sys

import click

from k2hackbot.global_classes import console


def convert_to_tuple_from_str(ctx, param, value):
    try:
        if type(value) == str:
            return [value]
        else:
            return list(value)
    except Exception as e:
        raise click.BadParameter('Unable to convert {} parameter to tuple'.format(param))


def json_provider(file_path, cmd_name):
    try:
        '''
        Populating default config
        '''
        path = os.path.join(os.path.dirname(__file__), "../config/default_config.json")

        with open(path, 'r') as f:
            config_data = f.read()
            config_dict = json.loads(config_data)
    except Exception as e:
        console.print_on_console("Please recheck default config in config/default_config.json. \nFound following issues in default Config file: {}".format(e))
        sys.exit(0)

    try:
        with open(file_path, 'r') as f:
            config_data = f.read()
            # updatings keys in config to lowercase
            user_config_dict = json.loads(config_data)
            user_config_keys = copy.deepcopy(list(user_config_dict.keys()))
            for config_option in user_config_keys:
                user_config_dict[config_option.lower()] = user_config_dict.pop(config_option)

        for config_option in user_config_dict.keys():
            config_dict[config_option] = user_config_dict[config_option]
        return config_dict
    except Exception as e:
        console.print_on_console("Please recheck config in {}. \nFound following issues in Config file: {}".format(file_path, e))
        sys.exit(0)
