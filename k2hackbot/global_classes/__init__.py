import os
import subprocess
import sys
from zipfile import ZipFile
import docker
import paramiko
from docker.errors import NotFound

from k2hackbot.config import project_config, user_config
from k2hackbot.console_handler import console
from k2hackbot.k2logger import logger


class MySFTPClient(paramiko.SFTPClient):
    def put_dir(self, source, target):
        ''' Uploads the contents of the source directory to the target path. The
            target directory needs to exists. All subdirectories in source are
            created under target.
        '''
        for item in os.listdir(source):
            if os.path.isfile(os.path.join(source, item)):
                self.put(os.path.join(source, item), '%s/%s' % (target, item))
            else:
                self.mkdir('%s/%s' % (target, item), ignore_existing=True)
                self.put_dir(os.path.join(source, item), '%s/%s' % (target, item))

    def mkdir(self, path, mode=511, ignore_existing=False):
        ''' Augments mkdir by adding an option to not fail if the folder exists  '''
        try:
            super(MySFTPClient, self).mkdir(path, mode)
        except IOError:
            if ignore_existing:
                pass
            else:
                raise


class ParamikoClient:
    def __init__(self):
        user_config_parameters = user_config.get_config_parameters()
        remote_machine_host, remote_machine_username, remote_machine_passw = user_config_parameters["remotemachinecredentials"].values()
        remote_host_port = 22
        self.remote_machine_username = remote_machine_username
        self.remote_machine_passw = remote_machine_passw
        self.remote_machine_ip = remote_machine_host
        self.remote_machine_port = remote_host_port
        self.ssh = self.get_ssh()
        self.file_path = os.path.dirname(__file__)

    def check_connection(self):
        try:
            ssh = self.get_ssh()
            if ssh:
                ssh.close()
                return True
        except:
            return False

    def get_ssh(self):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(self.remote_machine_ip, self.remote_machine_port, self.remote_machine_username, self.remote_machine_passw)
        return ssh

    def execute_on_remote(self, command):
        def return_formatted_data(string):
            # restricted keyword that are to be removed
            restricted_keywords = ["\r", "\t"]
            # String_set have set of all of the char
            string_set = set(string)
            # check for any unwanted string char
            has_unwanted_string_present = any([restricted_keyword in string_set for restricted_keyword in restricted_keywords])
            if has_unwanted_string_present:
                # filter unwanted_strings from collections
                unwanted_strings = filter(lambda x: x in string_set, restricted_keywords)
                # remove unwanted string from the original string
                for unwanted_string in unwanted_strings:
                    string = string.replace(unwanted_string, "")
            return string

        if not self.ssh:
            self.ssh = self.get_ssh()
        stdin, stdout, stderr = self.ssh.exec_command(command, get_pty=True)
        return return_formatted_data(stdout.read().decode("UTF-8"))

    def get_container_id(self, container_id):
        extraction_command = 'docker inspect --format="{{{{.Id}}}}" {}'.format(container_id)
        command_result = self.execute_on_remote(extraction_command)
        if "error" in command_result.lower():
            return ""
        else:
            whole_container_id = command_result[:12]
            return whole_container_id

    def get_container_status(self, container_id):
        extraction_command = ' docker inspect --format="{{json .State.Status}}"' + ' {}'.format(container_id)
        command_result = self.execute_on_remote(extraction_command)
        if "error" in command_result.lower():
            return False
        else:
            if "running" in command_result.lower():
                return True
            else:
                return False

    def run_on_container(self, container_id, exec_command):
        run_command_on_docker = 'docker exec {} bash -c "{}"'.format(container_id, exec_command)
        command_output = self.execute_on_remote(run_command_on_docker)
        return command_output

    def get_container_logs(self, container_id):
        docker_container_logs = "docker logs {}".format(container_id)
        container_logs = self.execute_on_remote(docker_container_logs)
        return container_logs

    def send_file_to_remote(self):
        transport = paramiko.Transport(self.remote_machine_ip, self.remote_machine_port)
        transport.connect(username=self.remote_machine_username, password=self.remote_machine_passw)
        sftp = MySFTPClient.from_transport(transport)

        sftp.mkdir('/tmp/k2install', ignore_existing=True)
        sftp.put_dir(self.file_path + "/../k2install", "/tmp/k2install")
        sftp.close()


class HostManager:
    def __init__(self):
        project_config_parameters = project_config.get_config_parameters()
        if 'intcode_host' in project_config_parameters and project_config_parameters['intcode_host'] == "remote":
            self.paramiko_client = ParamikoClient()

    def execute_on_local(self, command, stdout=False):
        command_file_logs = open("executed_command_logs", "a+")
        command_output = subprocess.run([command], shell=True, stdout=subprocess.PIPE)
        command_file_logs.close()
        output = command_output.stdout.decode("UTF-8")
        # console.print_on_console(output)
        return output

    def execute_on_host(self, command):
        project_config_parameters = project_config.get_config_parameters()
        if "intcode_host" in project_config_parameters and project_config_parameters["intcode_host"] == "remote":
            result = self.paramiko_client.execute_on_remote(command)
        else:
            result = self.execute_on_local(command)
        return result

    def extract_intcode_installer_zip(self):
        file_path = os.path.dirname(__file__)
        try:
            with ZipFile(file_path + "/../k2agent/vm-all.zip", 'r') as zip:
                # zip.printdir()
                zip.extractall(file_path + "/../")
            project_config_parameters = project_config.get_config_parameters()
            if project_config_parameters["intcode_host"] == "remote":
                self.paramiko_client.send_file_to_remote()
        except Exception as e:
            console.print_on_console("Following exception was occurred during unzipping the installer = {}".format(e))


class LocalDockerManager:
    def __init__(self):
        try:
            self.docker_client = docker.from_env()
        except Exception as e:
            console.print_on_console("Docker environment not found:")
            sys.exit(0)

    def get_container(self, container_id):
        try:
            return self.docker_client.containers.get(container_id)
        except NotFound:
            logger.debug("Container named {} not found".format(container_id))

    def get_container_id(self, container_id):
        # returns the containerID in truncated format
        container = self.get_container(container_id)
        if container:
            return container.id[:12]

    def get_container_status(self, container_id):
        # returns True if container is active
        container = self.get_container(container_id)
        if container:
            if container.status.lower() == "running":
                return True
            else:
                return False

    def run_on_container(self, container_id, exec_command):
        if self.get_container_status(container_id):
            container = self.get_container(container_id)
            final_command = ['/bin/bash', '-c', exec_command]
            return container.exec_run(final_command).output.decode("UTF-8")
        else:
            console.print_on_console("Container: {} is not active".format(container_id))
            sys.exit(0)

    def get_container_logs(self, container_id):
        if self.get_container_status(container_id):
            container = self.get_container(container_id)
            return container.logs().decode("UTF-8")


class DockerManager:
    def __init__(self):
        project_config_parameters = project_config.get_config_parameters()
        if project_config_parameters["intcode_host"] == "remote":
            self.docker_client = ParamikoClient()
        else:
            self.docker_client = LocalDockerManager()


def page_is_loading(driver):
    while True:
        x = driver.execute_script("return document.readyState")
        if x == "complete":
            return True
        else:
            yield False
