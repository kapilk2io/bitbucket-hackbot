import json
import logging.config
import os
import os


class K2Logger:
    def __init__(self):
        # Run once at startup:
        # Include in each module:
        self.LOGGERCONFIG = {
            "version": 1,
            "disable_existing_loggers": True,
            "formatters": {
                "standard": {
                    "format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s"
                }
            },
            "handlers": {
                "default": {
                    "level": "DEBUG",
                    "formatter": "standard",
                    "class": "logging.StreamHandler",
                    "stream": "ext://sys.stdout"
                },
                "fh": {
                    "class": "logging.handlers.RotatingFileHandler",
                    "formatter": "standard",
                    "level": "DEBUG",
                    "filename": os.path.join(os.path.dirname(__name__), "myfile.log"),
                    "backupCount": 20
                }
            },
            "loggers": {
                "": {
                    "handlers": [
                        "fh"
                    ],
                    "level": "DEBUG",
                    "propagate": False
                }
            }
        }
        logging.config.dictConfig(self.LOGGERCONFIG)
        self.log = logging.getLogger("K2HackBot")
        self.log.debug("K2HackBot Initiated")

    def get_logger(self):
        return self.log
