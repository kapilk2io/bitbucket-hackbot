import sys
from time import sleep
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from k2hackbot.config import project_config
from k2hackbot.global_classes import HostManager, DockerManager
from k2hackbot.k2logger import logger
from selenium.webdriver.remote.remote_connection import LOGGER
from k2hackbot.console_handler import console

LOGGER.setLevel(50)


class BrowserDriver(HostManager):
    def __init__(self):
        super().__init__()
        self.docker_client = DockerManager().docker_client
        self.browser_container_name = "k2crawlertool"
        self.setup_stand_alone_chrome_container = "docker run -d -p 4444:4444  -p 7900:7900  -p 5900:5900 -v /dev/shm:/dev/shm --name {} selenium/standalone-chrome:4.0.0-beta-3-20210426 > /dev/null 2>&1".format(self.browser_container_name)
        self.driver = None
        self.setup_browser = not self.check_existing_browser_container()
        self._chrome_option = self.get_chrome_option()
        self._project_config_parameters = project_config.get_config_parameters()
        self._restart_cmd = "docker restart {}".format(self.browser_container_name)

    def get_chrome_option(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        options.add_argument("--lang=de")
        options.add_argument('--disable-notifications')
        # options.headless = True
        return options

    def setup_host_chrome_connection(self, setup):
        console.print_with_animation("Setting Up Local k2crawlertool Environment", "Local Environment Setup ")
        try:
            local_chrome_driver = ChromeDriverManager(print_first_line=False).install()
            project_config.set_config_parameter("chromedriver", local_chrome_driver)
            driver = webdriver.Chrome(self._project_config_parameters["chromedriver"], options=self._chrome_option)
            console.stop_printing_animation(result=True)
        except:
            console.stop_printing_animation(result=False)
            driver = self.setup_remote_chrome_connection(setup)
        return driver

    def setup_remote_chrome_connection(self, setup):
        console.print_with_animation("Setting Up Dockerize k2crawlertool Environment", "Dockerize Environment Setup ")
        if setup:
            self.execute_on_host(self.setup_stand_alone_chrome_container)

        if not self.get_status_for_existing_browser_container():
            if not self.restart_browser_container():
                console.stop_printing_animation(result=False)
                console.print_on_console("Unable to restart the container named {}. Please remove the container manually.".format(self.browser_container_name))
                sys.exit(0)

        sleep(5)
        project_config_parameters = project_config.get_config_parameters()
        driver = webdriver.Remote(project_config_parameters["chromedriver"], self._chrome_option.to_capabilities())
        console.stop_printing_animation(result=True)
        return driver

    def setup_browser_container(self):
        project_config_parameters = project_config.get_config_parameters()
        if 'intcode_host' in project_config_parameters and project_config_parameters['intcode_host'] == "remote":
            self.driver = self.setup_remote_chrome_connection(self.setup_browser)
        else:
            self.driver = self.setup_host_chrome_connection(self.setup_browser)

    def restart_browser_container(self):

        self.execute_on_host(self._restart_cmd)
        sleep(10)
        browser_container_restart_status = self.get_status_for_existing_browser_container()
        return browser_container_restart_status

    def check_existing_browser_container(self):
        container_id = self.docker_client.get_container_status(self.browser_container_name)
        if container_id:
            return True
        else:
            return False

    def get_status_for_existing_browser_container(self):
        if self.check_existing_browser_container():
            return True
        return False


if __name__ == '__main__':
    project_config.set_config_parameter("intcode_host", "host")
    project_config.set_config_parameter("k2_hack_bot_environment", "console")
