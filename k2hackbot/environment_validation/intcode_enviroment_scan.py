import sys

import click

from k2hackbot.config import user_config, project_config
from k2hackbot.console_handler import console
from k2hackbot.environment_validation.enviroment_scanner import EnvironmentScanner
from k2hackbot.k2manager import K2Manager


class IntCodeEnvironmentScan(EnvironmentScanner):
    def __init__(self):
        super().__init__()
        self.k2_manager = K2Manager()

    def check_remote_host_environment(self):
        from k2hackbot.config import  project_config
        project_config_parameters = project_config.get_config_parameters()
        check_methods = {
            "Checking connectivity to Remote Machine": self._check_connection_to_remote_machine,
            "Verifying Docker Environment ": self._check_docker_connection,
            "Checking connectivity with Dockerhub Portal ": self._check_docker_hub_connection,
            "Checking status of required Ports ": self._check_ports_status,
            "Checking connectivity to {}".format(project_config_parameters["k2cloud"]): self._check_k2m_user
        }
        return self.test_case_checker(check_methods)

    def _check_k2m_user(self):
        # TODO update logic to use request module to check
        cookies = self.k2_manager.create_customer_session()
        if not cookies[0]:
            click.echo('ABORTING TEST: Invalid K2M username/password. Please verify it once')
            return False
        else:
            return True

    def _check_connection_to_k2io_portal(self):
        result = self.k2_manager.check_connection()
        return True if result else False

    def check_local_host_environment(self):
        from k2hackbot.config import project_config
        project_config_parameters = project_config.get_config_parameters()
        user_config_parameters = user_config.get_config_parameters()
        check_methods = {
            "Verifying Docker Environment ": self._check_docker_connection,
            "Checking connectivity to {}".format(project_config_parameters["k2cloud"]): self._check_k2m_user,
            "Checking connectivity with Dockerhub Portal ": self._check_docker_hub_connection,
            "Checking status of required Ports ": self._check_ports_status,
            f"Verifying {user_config_parameters['k2email']} on {project_config_parameters['k2cloud']}": self._check_k2m_user

        }
        return self.test_case_checker(check_methods)

    def check_user_policy(self):
        user_config_parameters = user_config.get_config_parameters()
        console.print_with_animation("Checking K2 Agent Configuration", "K2 Agent Configuration Verification")
        agent_config = self.k2_manager.get_policy_info(policy_type='agentConfig', group_name=user_config_parameters["k2groupname"])

        if agent_config[0] and "dynamicAttachment" in agent_config[1]:
            console.stop_printing_animation()
            if not agent_config[1]["dynamicAttachment"]:
                console.print_on_console("Dynamic Attachment for {} group policy is False, Application can only be attached Statically".format(user_config_parameters["k2groupname"]))
        elif "dynamicAttachment" not in agent_config[1]:
            console.stop_printing_animation()
            console.print_on_console("Something went wrong for {} group policy !!\n {}".format(user_config_parameters["k2groupname"], agent_config))
            sys.exit(0)
        else:
            console.stop_printing_animation()
            console.print_on_console("Unable to fetch policies from K2io.net !!, Please c")
            sys.exit(0)

        agent_policy = self.k2_manager.get_policy_info(policy_type='agentPolicy', group_name=user_config_parameters["k2groupname"])
        if agent_policy[0] and agent_policy[1]['iastMode']["enabled"] and agent_policy[1]['iastMode']['dynamicScanning']["enabled"]:
            # console.stop_printing_animation()
            pass
        elif not agent_policy[1]['iastMode']["enabled"]:
            console.print_on_console("Please check User configuration for {} group policy Please set IAST mode to True".format(user_config_parameters["k2groupname"]))
            sys.exit(0)
        else:
            # console.stop_printing_animation()
            console.print_on_console("Please check User configuration for {} group policy Please set dynamic scanning to True".format(user_config_parameters["k2groupname"]))
            sys.exit(0)

        agent_deployment_env = self.k2_manager.get_group_deployed_env_info()
        if agent_deployment_env[0]:
            for policy in agent_deployment_env[1]:
                if policy["name"] == user_config_parameters["k2groupname"] and policy["agentDeploymentEnvironment"] == "PRODUCTION":
                    console.print_on_console("Group policy named {} has PRODUCTION as deployment environment. Please use a group policy with deployment environment different from PRODUCTION".format(user_config_parameters["k2groupname"]))
                    sys.exit(0)

    def check(self):
        """
        Master Method to Validate Environment Requirement for K2Component to Setup
        """

        console.empty_lines()
        console.print_on_console("Checking Environment", style=console.BOLD)

        # Check Host Related Validations
        project_config_parameters = project_config.get_config_parameters()
        if "intcode_host" in project_config_parameters and project_config_parameters["intcode_host"] == "remote":
            self.check_remote_host_environment()
        else:
            self.check_local_host_environment()

        # Policy Related Validations
        self.check_user_policy()
        # validate deployment environment of policy as DS doesn't work in PRODUCTION
