import sys

from k2hackbot.config import project_config, user_config
from k2hackbot.environment_validation.enviroment_scanner import EnvironmentScanner
from k2hackbot.console_handler import console



class CrawlerEnvironmentScan(EnvironmentScanner):
    def __init__(self):
        super().__init__()
        user_config_parameters = user_config.get_config_parameters()
        self.application_url = user_config_parameters["applicationurl"]

    def check_user_login_and_save_cookies(self):
        from k2hackbot.k2crawler.auth_user import AuthUser
        auth_user = AuthUser()
        user_config_parameters = user_config.get_config_parameters()
        form_element_identification = user_config_parameters["applicationloginidentifier"]
        app_url = user_config_parameters["applicationloginurl"]
        status, cookies = auth_user.login_using_provided_identifier(app_url, form_element_identification)
        if status:
            console.print_center_text_without_heading("Logged in ")
            project_config.set_config_parameter("applicationcookies", str(cookies))
            return True
        else:
            console.print_on_console("Failed To Login Please check Credential You have Provided")
            sys.exit(0)

    def check(self):
        user_config_parameters = user_config.get_config_parameters()
        if user_config_parameters['isauthrequired'] and user_config_parameters['applicationloginurl'] not in self.application_url:
            check_methods = {"Checking connectivity to {}".format([user_config_parameters['applicationloginurl']] + self.application_url): self._check_url_connection}
        else:
            check_methods = {"Checking connectivity to {}".format(self.application_url): self._check_url_connection}
        if user_config_parameters["isauthrequired"]:
            check_methods["Logging in as Authenticated User"] = self.check_user_login_and_save_cookies
        return self.test_case_checker(check_methods)
