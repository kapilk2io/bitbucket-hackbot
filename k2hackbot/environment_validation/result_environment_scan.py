from k2hackbot.console_handler import console

from k2hackbot.environment_validation.enviroment_scanner import EnvironmentScanner


class ResultEnvironmentScan(EnvironmentScanner):
    def __init__(self):
        super().__init__()

    def check(self):
        self.check_intcode_container()

    def check_intcode_container(self):
        """
        Checks for intcode container
        :return: bool
        """
        try:
            console.print_with_animation("Checking for K2Agent Container", "Found K2agent Container")
            self.docker_manager.docker_client.get_container_id("k2agent")
            console.stop_printing_animation()
            return True
        except:
            console.stop_printing_animation(result=False)
            return False
