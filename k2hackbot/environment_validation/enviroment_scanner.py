import sys

import click
import requests

from k2hackbot.config import user_config
from k2hackbot.console_handler import console

from k2hackbot.global_classes import ParamikoClient, HostManager, DockerManager
from k2hackbot.k2agent import K2Agent


class EnvironmentScanner:
    def __init__(self):
        self.HostManagerObj = HostManager()
        self.docker_manager = DockerManager()

    def _check_environment_variable(self):
        pass

    def _check_sys_variable(self):
        pass

    def _check_docker_hub_connection(self):
        docker_hub_url = "https://hub.docker.com/"
        try:
            result = requests.get(docker_hub_url)
            response_code = result.status_code
            if response_code == 200:
                return self._check_connection_with_k2_repo()
            else:
                console.print_on_console("Received following response code from {} : {}".format(docker_hub_url, response_code))
                return False
        except:
            return False

    def _check_connection_with_k2_repo(self):
        k2_repo_url = "https://hub.docker.com/r/k2cyber/k2-agent-v1"
        try:
            result = requests.get(k2_repo_url)
            response_code = result.status_code
            if response_code == 200:
                return True
            else:
                console.print_on_console("Received following response code from {} : {}".format(k2_repo_url, response_code))
                return False
        except:
            return False

    def _check_ports_status(self):
        try:
            from k2hackbot.k2browser.browser_driver import BrowserDriver
            k2agent_container_status = K2Agent().check_installed_intcode_agent()
            k2crawlertool_container_status = not BrowserDriver().setup_browser
            if not (k2agent_container_status and k2crawlertool_container_status):
                tcp_busy_ports_command = "ss -tulpn | grep tcp | awk '{print $5}'"
                output = self.HostManagerObj.execute_on_host(tcp_busy_ports_command)
                busy_ports = [int(local_address.rstrip("\r").split(":")[-1]) for local_address in output.split("\n")[:-1]]
                # IC requires 54321, 54322, 54323
                # HackBot requires 4444, 7900, 5900
                required_ports = []
                if not k2agent_container_status:
                    required_ports += [54321, 54322, 54323]
                if not k2crawlertool_container_status:
                    required_ports += [4444, 7900, 5900]
                for required_port in required_ports:
                    if required_port in busy_ports:
                        console.print_on_console("Following port is already in use: {}".format(required_port))
                        return False
                return True
            else:
                return True
        except Exception as e:
            console.print_on_console("Following exception generated during checking required ports status:\n{}".format(e))
            return False

    def _check_url_connection(self) -> bool:
        total_working_url = []
        user_config_parameters = user_config.get_config_parameters()

        from k2hackbot.k2browser import browser_driver
        complete_application_urls = []
        if user_config_parameters['isauthrequired']:
            complete_application_urls = [user_config_parameters['applicationloginurl']]
        complete_application_urls += user_config_parameters['applicationurl']

        for url in complete_application_urls:
            try:
                browser_container_status = browser_driver.get_status_for_existing_browser_container()
                if browser_container_status:
                    # check the working of curl from k2crawler curl
                    curl_request = "curl -k -s -o /dev/null -sw '%{{http_code}}' {}".format(url)
                    response_code = int(self.docker_manager.docker_client.run_on_container(browser_driver.browser_container_name, curl_request))
                else:
                    response = requests.get(url, verify=False)
                    response_code = response.status_code
            except Exception as e:
                console.stop_printing_animation(result=False)
                continue
            if response_code >= 400 or response_code < 200:
                console.stop_printing_animation(result=False)
                console.print_on_console("Received following status code for URL {}: {}".format(url, response_code))
                if user_config_parameters['isauthrequired'] and url == user_config_parameters['applicationloginurl']:
                    console.print_on_console("Please check application login URL.")
                    sys.exit(0)
            else:
                if url != user_config_parameters['applicationloginurl']:
                    total_working_url.append(url)

        console.stop_printing_animation(result=True)
        if total_working_url:
            if len(total_working_url) != len(user_config_parameters['applicationurl']):
                user_config_parameters['applicationurl'] = total_working_url
                user_config.set_config_parameters(user_config_parameters)
            return True
        else:
            console.print_on_console("NO Url to work with exiting the process")
            sys.exit()

    def _check_docker_connection(self):
        docker_api_version_command = '''docker version --format "{{.Client.APIVersion}}" 2> /dev/null'''
        output = self.HostManagerObj.execute_on_host(docker_api_version_command)
        if output:
            return True
        else:
            console.stop_printing_animation(result=False)
            console.print_on_console("Did Not Found Docker Environment Please check your "
                                     "Environment")
            sys.exit(0)

    def _check_connection_to_remote_machine(self):
        result = ParamikoClient().check_connection()
        return True if result else False

    def test_case_checker(self, test_dict, *args, **kwargs):
        for key, check_method in test_dict.items():
            console.print_with_animation(key, key)
            if not check_method():
                console.stop_printing_animation(result=False)
                sys.exit()
            else:
                console.stop_printing_animation(result=True)
        return True

    def check(self):
        pass
