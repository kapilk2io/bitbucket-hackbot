import itertools
import shutil
import time
from functools import wraps
import threading
import click
import pyfiglet
from k2hackbot.config.config import ProjectConfig
from k2hackbot.k2logger import logger


class ConsoleOut:
    def __init__(self):
        self.status = ""
        self.terminal_dimension = shutil.get_terminal_size(fallback=(120, 50))
        self.project_config = ProjectConfig()
        self.run_animation = True
        self.animate_thread = None
        self.slash_itercycle = itertools.cycle(['|', '/', '-', '\\'])
        self.dot_itercycle = itertools.cycle(['.   ', '..  ', '... ', '....'])
        self.progress_end_value = ""
        self.PURPLE = '\033[95m'
        self.CYAN = '\033[96m'
        self.DARKCYAN = '\033[36m'
        self.BLUE = '\033[94m'
        self.GREEN = '\033[92m'
        self.YELLOW = '\033[93m'
        self.RED = '\033[91m'
        self.BOLD = '\033[1m'
        self.UNDERLINE = '\033[4m'
        self.END = '\033[0m'
        self.tick = '\u2713'
        self.cross = '\u2718'

    def __del__(self):
        self.run_animation = False
        del self.animate_thread

    def print_on_console(self, output, style=None, **kwargs):
        def pprint(word, **kwargs):
            try:
                print("\r" + word, **kwargs)
                logger.debug(word)
            except UnicodeEncodeError:
                print(word.encode("utf-8"), **kwargs)

        if style:
            output = style + output + self.END
        if type(output) == str:
            pprint(output, **kwargs)

        elif type(output) == list:
            [pprint(i) for i in output]

    def center_wrap(self, text: list, **kw):
        whole_string = ""
        for lines in text:
            line = " " * (self.terminal_dimension.columns // 2 - len(lines) // 2) + lines + "\n"  # textwrap.wrap(lines, **kw)
            whole_string += line
        return whole_string

    def print_decorated_heading(self, heading):
        self.section_breaker()
        self.print_center_text_without_heading(heading)
        self.section_breaker()

    def print_center_text_without_heading(self, text):
        if type(text) == str:
            text = [text]
        self.print_on_console(self.center_wrap(text))

    def print_pyglet_heading(self, text: str, center=True):
        text = " ".join(list(text))
        decorated_text = pyfiglet.figlet_format(text)
        decorated_line = decorated_text.splitlines()
        if center:
            self.print_center_text_without_heading(decorated_line)
        else:
            self.print_on_console(decorated_line)

    def welcome_greeting_for_user(self):
        self.section_breaker()
        # self.print_on_console(pyfiglet.figlet_format("K2 Hack Me")) # .center(shutil.get_terminal_size().columns//2)
        self.print_on_console("""
                                        {} _  __{}  ____     _   _            _      ____        _
                                        {}| |/ /{} |___ \   | | | | __ _  ___| | __ | __ )  ___ | |_
                                        {}| ' / {}   __) |  | |_| |/ _` |/ __| |/ / |  _ \ / _ \| __|
                                        {}| . \ {}  / __/   |  _  | (_| | (__|   <  | |_) | (_) | |_
                                        {}|_|\_\{} |_____|  |_| |_|\__,_|\___|_|\_\ |____/ \___/ \__|
                                                                         A K2 Cyber Security Offering
                                                                         Copyright K2 Cyber Security, © 2021
        """.format(*[self.BLUE, self.END] * 5))
        self.section_breaker()
        self.print_on_console("""
        K2HackBot is an offering of K2 Cyber Security, Inc, to help you secure your Web applications and APIs.
        K2HackBot tool will test if your Web application or APIs can be easily hacked.
        And will show you vulnerable url/APIs that can be hacked along with proof provided of one
        """)
        self.section_breaker()
        self.empty_lines()

    def section_breaker(self):
        self.print_on_console("#" * self.terminal_dimension.columns)

    def empty_lines(self):
        # self.print_on_console(" " * self.terminal_dimension.columns)
        self.print_on_console(" " * self.terminal_dimension.columns)

    def animate(self, start, end="", style="slash"):
        project_config_parameters = self.project_config.get_config_parameters()
        if project_config_parameters["k2_hack_bot_environment"] == "jenkins":
            cycle = ["", " "]
            time_delay = 0.1
        else:
            if style == "dot":
                cycle = self.dot_itercycle
            else:
                cycle = self.slash_itercycle
            time_delay = 0.1
        for c in cycle:
            if not self.run_animation:
                self.run_animation = True
                if end:
                    en_str_length = len(end) + 8
                    end_output = "> {} {} {}".format(end, self.status, " " * (self.terminal_dimension.columns-en_str_length))
                    print("\r   {}".format(end_output), flush=True)
                    logger.debug(end_output)
                else:
                    print("\r{}".format(" " * self.terminal_dimension.columns))
                self.status = ""
                self.progress_end_value = ""
                break
            print("\r   > {} ".format(start) + c + str(self.progress_end_value), end="")
            time.sleep(time_delay)

    def print_with_animation(self, start, end="", style=""):
        project_config_parameters = self.project_config.get_config_parameters()
        if self.run_animation:
            self.run_animation = False
            time.sleep(.2)
            self.run_animation = True
        elif project_config_parameters["k2_hack_bot_environment"] != "jenkins":
            self.run_animation = True
        self.animate_thread = threading.Thread(target=self.animate, args=(start, end, style))

        self.animate_thread.daemon = True
        self.animate_thread.start()
        return self.animate_thread

    def set_status(self, result):
        if result:
            self.status = self.tick
        else:
            self.status = self.cross

    def stop_printing_animation(self, result=True):
        self.run_animation = False
        self.set_status(result)
        time.sleep(2)

    def function_console_wrapper(self, start, end=None):
        def somedec_outer(fn):
            @wraps(fn)
            def somedec_inner(*args, **kwargs):
                self.print_with_animation(start, end)
                response = fn(*args, **kwargs)
                self.stop_printing_animation()
                return response

            return somedec_inner

        return somedec_outer

    def get_user_input(self, prompt_message):
        user_input = click.prompt(prompt_message, type=str)
        return user_input

    def get_user_conformation(self, conformation_message):
        user_input = click.confirm(conformation_message)
        return user_input



