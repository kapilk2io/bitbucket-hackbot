# Todo Update K2manager file for k2manager methods
import click
import requests

from k2hackbot.global_classes import HostManager


class K2Zap(HostManager):
    def __init__(self, url):
        self.url = url

    def setup_zap_container(self):
        """
        This module will run the ZAP proxy container
        """
        zap_run_command = '''docker run --detach --name zap -u zap -v "$(pwd)/reports":/zap/reports/:rw \
                              -i owasp/zap2docker-stable zap.sh -daemon -host 0.0.0.0 -port 8080 \
                              -config api.addrs.addr.name=.* -config api.addrs.addr.regex=true \
                              -config api.disablekey=true'''

        output = self.execute_on_host(zap_run_command)
        # click.echo(f'ZAP: {status}')
        click.echo(f'ZAP output: {output.stdout}')
        return True

    def verify_zap_setup(self):
        """
        This module verify if application is reachable from inside the ZAP container.
        This verification is done before starting the ZAP daemon.
        """

        curl_command = '''curl -i {}'''.format(self.url)
        output = self.execute_on_host(curl_command)
        # click.echo(f'ZAP: {status}')
        click.echo(f'ZAP output: {output.stdout}')
        return True

    def start_scanning(self):
        """
        This module starts the ZAP daemon to run API scanning
        """

        start_zap_daemon_command = '''docker exec zap bash -c "sleep1;zap.sh -daemon -quickurl {}"'''.format(self.url)
        click.echo(start_zap_daemon_command)
        output = self.execute_on_host(start_zap_daemon_command)
        # click.echo(f'ZAP: {status}')
        click.echo(f'ZAP output: {output.stdout}')
        return True
