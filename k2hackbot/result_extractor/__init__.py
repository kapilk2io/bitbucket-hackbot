import json
import sys
from time import time, sleep
import re

from k2hackbot.config import user_config
from k2hackbot.console_handler import console

from k2hackbot.global_classes import HostManager, DockerManager
from k2hackbot.k2manager import K2Manager
from columnar import columnar


class ResultViewer(HostManager):
    def __init__(self):
        super().__init__()
        self.k2manger = K2Manager()
        self.docker_manager = DockerManager().docker_client
        self.total_number_vulnerable_api = 0
        self.applicationuuid = self.get_application_uuid()

    def check_appinfo_for_applicationidentifier(self,info, application_identifier_json):
        if "containerId" in info['value']['sourceIdentifier']:
            if "containerid" in application_identifier_json.keys() and "pid" in application_identifier_json.keys():
                if (application_identifier_json["containerid"] == info['value']['sourceIdentifier']["containerId"][:len(application_identifier_json["containerid"])] and str(application_identifier_json["pid"]) == str(info['value']["pid"])):
                    return True
            elif "containerid" in application_identifier_json:
                if application_identifier_json["containerid"] == info['value']['sourceIdentifier']["containerId"][:len(application_identifier_json["containerid"])]:
                    return True
            elif "pid" in application_identifier_json:
                if application_identifier_json["pid"] == str(info['value']["pid"]):
                    return True
        elif "pid" in info['value']:
            if "pid" in application_identifier_json and application_identifier_json["pid"] == str(info['value']["pid"]):
                    return True
        return False

    def get_application_uuid(self):
        def check_for_multiple_records_for_same_containerid(container_id, application_info_record):
            matched_records_count = 0
            for record in application_info_record:
                if "containerId" in record['value']['sourceIdentifier'] and container_id == record['value']['sourceIdentifier']["containerId"][:len(container_id)]:
                    matched_records_count += 1

            return matched_records_count > 1

        try:
            application_info_data_content = self.docker_manager.run_on_container("k2agent", "java -jar /IntcodeDBDumper-jar-with-dependencies.jar /K2IntCode.db /dump-apirecord.json ApplicationInfoData ;echo 'Break Here' ;cat /dump-apirecord.json").split("Break Here")[1]
            application_info_data = json.loads(application_info_data_content)
            application_info = application_info_data['collections'][0]['data']
            user_config_parameters = user_config.get_config_parameters()
            application_identifier_json = user_config_parameters["applicationidentifier"]
            for info in application_info:
                if self.check_appinfo_for_applicationidentifier(info,application_identifier_json):
                    if "containerid" in application_identifier_json and check_for_multiple_records_for_same_containerid(application_identifier_json["containerid"], application_info):
                        console.print_on_console("Found multiple records for containerId {}. Please provide complete containerId for application".format(application_identifier_json["containerid"]))
                        sys.exit(0)
                    return info['value']['applicationUUID']
        except Exception as e:
            console.print_on_console("Following exception occurred during extracting the application UUID:\n{}".format(e))
            sys.exit(0)

    def wait_and_scan_for_vulnerability_result(self):
        initial_time = time()
        console.progress_end_value = 0
        console.run_animation = True
        console.print_with_animation("Number of Exploits discovered", style="dot")
        console.progress_end_value = self.number_of_vulnerability_found()
        # print(console.progress_end_value)
        while time() - initial_time < 1600:
            if self.check_for_ds_scan_file():
                break
            else:
                total_number_of_vulnerability_found = self.number_of_vulnerability_found()
                console.progress_end_value = total_number_of_vulnerability_found
                # print(console.progress_end_value)
                sleep(1)
        console.progress_end_value = ""
        console.stop_printing_animation()
        self.total_number_vulnerable_api = self.number_of_vulnerability_found()

    def prompt_discovered_vulnerability(self):

        cve_data = self.get_cve_results()['data']
        container_vulnerabilities = self.get_container_vulnerability_scan_result()['data']
        cve_and_container_information = ""
        if len(cve_data) > 0 and len(container_vulnerabilities):
            if len(cve_data) > 0:
                cve_and_container_information = "And {} CVE".format(len(cve_data))
            if len(container_vulnerabilities):
                cve_and_container_information += " And {} Container Vulnerabilities".format(len(cve_data), len(container_vulnerabilities))
            cve_and_container_information += " discovered"
        console.print_pyglet_heading(str(self.total_number_vulnerable_api) + " Exploits\nDiscovered")
        console.print_center_text_without_heading(cve_and_container_information)

    def prompt_table_on_console(self, headers, data):
        table = columnar(data, headers=headers, justify=['l', 'c', 'c'])  # , min_column_width=30, patterns=patterns)
        console.print_on_console(table)

    def check_for_ds_scan_file(self):
        list_for_dir = self.docker_manager.run_on_container("k2agent", "ls /opt/k2-ic/scan-results")
        # console.print_on_console("these are list of dir {}".format(list_for_dir))
        if "ds-inactive" in list_for_dir:
            return True
        else:
            return False

    def get_vulnerable_api_record(self):
        all_api_records = self.docker_manager.run_on_container("k2agent", f"cat /tmp/k2logs/k2-vulnerability-record.log | grep 'Vulnerable API detected :' | grep '{self.applicationuuid}'").splitlines()
        return all_api_records

    def number_of_vulnerability_found(self):
        all_api_records = self.get_vulnerable_api_record()
        filtered_api_records = list(map(lambda x: re.findall('\"apiId.*?\",', x)[0], all_api_records))
        unique_api_id = []
        for api_id in filtered_api_records:
            if api_id not in unique_api_id:
                unique_api_id.append(api_id)
        total_number_of_unique_id = len(unique_api_id)
        return total_number_of_unique_id

    def extract_user_code_vuln_scan_data(self):
        try:
            retry_count = 0
            while retry_count < 3:
                user_code_vuln_scan_json = json.loads(self.docker_manager.run_on_container("k2agent", "cat /opt/k2-ic/scan-results/user-code-vuln-scan/{}".format(self.applicationuuid)))
                user_code_vuln_scan = []
                if "data" in user_code_vuln_scan_json:
                    user_code_vuln_scan = user_code_vuln_scan_json['data']
                if len(user_code_vuln_scan) >= self.total_number_vulnerable_api:
                    return True, user_code_vuln_scan
                retry_count += 1
                sleep(2)
            console.print_on_console("Failed to fetch data from /opt/k2-ic/scan-results/user-code-vuln-scan/{}. Performed 3 retries.".format(self.applicationuuid))
            return False, []
        except Exception as e:
            console.print_on_console("Following exception generated during fetching data from /opt/k2-ic/scan-results/user-code-vuln-scan/{}.\n{}".format(self.applicationuuid, e))
            return False, []

    def prompt_vulnerability_scan_result(self):
        # TODO get applicationuuid and container ID correlation from IC-Team

        try:
            if self.total_number_vulnerable_api > 0:
                console.run_animation = False
                console.print_with_animation("Preparing Exploit Report", style="dot")
                vulnerable_api_record = {}
                list_dir = self.docker_manager.run_on_container("k2agent", "ls /opt/k2-ic/scan-results/user-code-vuln-scan/")
                if self.applicationuuid in list_dir:
                    extraction_status, all_user_code_vuln_scan = self.extract_user_code_vuln_scan_data()
                    if not extraction_status:
                        console.stop_printing_animation(result=False)
                        console.print_on_console("File named {} not found inside /opt/k2-ic/scan-results/user-code-vuln-scan directory".format(self.applicationuuid))
                        return
                    for vulnerability_record in all_user_code_vuln_scan:
                        vulnerable_url = vulnerability_record['url']
                        attack_types = vulnerability_record['vulnerabilityCaseType']
                        api_id = vulnerability_record['apiId']
                        if vulnerable_url not in vulnerable_api_record:
                            vulnerable_api_record[vulnerable_url] = {}
                        if api_id not in vulnerable_api_record[vulnerable_url]:
                            vulnerable_api_record[vulnerable_url][api_id] = {}
                        if attack_types not in vulnerable_api_record[vulnerable_url][api_id]:
                            vulnerable_api_record[vulnerable_url][api_id][attack_types] = 1

                    data = []
                    for url, api_record in vulnerable_api_record.items():
                        record = [url]
                        all_vulnerability = ""
                        for api in api_record.values():
                            for vulnerability in api.keys():
                                all_vulnerability += vulnerability + ","
                        record.append(all_vulnerability)
                        data.append(record)
                    sleep(5)
                    console.stop_printing_animation()
                    console.print_on_console("Discovered Exploits", style=console.BOLD)
                    headers = ['Vulnerable URI/API', 'Vulnerability Type']
                    self.prompt_table_on_console(headers, data)
                else:
                    console.stop_printing_animation()
                    console.print_on_console("File named {} not found in /opt/k2-ic/scan-results/user-code-vuln-scan directory".format(self.applicationuuid))
                    console.print_on_console("Directory(/opt/k2-ic/scan-results/user-code-vuln-scan) content: \n{}".format(list_dir))
        except Exception as e:
            console.print_on_console("Exception occurred while prompting the vulnerability scan result: {}".format(e))

    def get_cve_results(self):
        cve_scan_result = self.docker_manager.run_on_container("k2agent", "cat /opt/k2-ic/scan-results/cve-scan/{}".format(self.applicationuuid))
        try:
            return json.loads(cve_scan_result)
        except:
            return {"draw": None, "recordsFiltered": 0, "recordsTotal": 0, "data": []}

    def prompt_cve_vulnerable_result(self):
        application_cve_scan_result = self.get_cve_results()['data']
        if application_cve_scan_result:
            console.empty_lines()
            console.print_on_console("Discovered Common Vulnerabilities and Exposures ", style=console.BOLD)
            console.empty_lines()
            sorted_cve_list = sorted(application_cve_scan_result, key=lambda k: k["severity"])[:4]
            headers = ["CVE", "Severity", "Component"]
            data = []
            for cve in sorted_cve_list:
                data.append([cve["cve"], cve["severity"], cve["component"]])
            self.prompt_table_on_console(headers, data)

    def print_report_message(self):
        if self.total_number_vulnerable_api:
            msg = """K2hackMe tool identified vulnerable URIs/APIs in your application."""
        else:
            msg = """Your application was found safe by K2hackMe bot."""
        console.empty_lines()
        console.print_center_text_without_heading(msg)

    def prompt_container_vulnerability_result(self):
        container_vulnerability_scan_result = self.get_container_vulnerability_scan_result()["data"]
        if container_vulnerability_scan_result:
            console.print_on_console("Discovered Container Vulnerability")
            container_vulnerability_scan_result = sorted(container_vulnerability_scan_result, key=lambda k: k["severity"])[:4]
            data = []
            for container_scan in container_vulnerability_scan_result:
                cve = container_scan["cve"]
                severity = container_scan["severity"]
                image_id = container_scan['imageId']
                data.append([cve, severity, image_id])
            headers = ["CVE", "Severity", "Image ID"]
            self.prompt_table_on_console(headers, data)

    def get_container_vulnerability_scan_result(self):
        cve_scan_result = self.docker_manager.run_on_container("k2agent", "cat /opt/k2-ic/scan-results/container-scan/{}".format(self.applicationuuid))
        try:
            return json.loads(cve_scan_result)
        except:
            return {"draw": None, "recordsFiltered": 0, "recordsTotal": 0, "data": []}
