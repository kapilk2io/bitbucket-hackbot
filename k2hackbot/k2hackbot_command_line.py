#!/usr/bin/python3
import os
import sys
import click
import click_config_file
import urllib3
from k2hackbot.global_classes.click_configurations import convert_to_tuple_from_str, json_provider
from k2hackbot.console_handler import console

urllib3.disable_warnings()
'''
RuntimeError: Click will abort further execution because Python 3 was configured to use ASCII as encoding for the environment. Consult https://click.palletsprojects.com/python3/ for mitigation steps.
This system lists a couple of UTF-8 supporting locales that you can pick from. The following suitable locales were discovered: af_ZA.UTF-8, am_ET.UTF-8, be_BY.UTF-8, bg_BG.UTF-8, ca_ES.UTF-8, cs_CZ.UTF-8, da_DK.UTF-8, de_AT.UTF-8, de_CH.UTF-8, de_DE.UTF-8, el_GR.UTF-8, en_AU.UTF-8, en_CA.UTF-8, en_GB.UTF-8, en_IE.UTF-8, en_NZ.UTF-8, en_US.UTF-8, es_ES.UTF-8, et_EE.UTF-8, eu_ES.UTF-8, fi_FI.UTF-8, fr_BE.UTF-8, fr_CA.UTF-8, fr_CH.UTF-8, fr_FR.UTF-8, he_IL.UTF-8, hr_HR.UTF-8, hu_HU.UTF-8, hy_AM.UTF-8, is_IS.UTF-8, it_CH.UTF-8, it_IT.UTF-8, ja_JP.UTF-8, kk_KZ.UTF-8, ko_KR.UTF-8, lt_LT.UTF-8, nl_BE.UTF-8, nl_NL.UTF-8, no_NO.UTF-8, pl_PL.UTF-8, pt_BR.UTF-8, pt_PT.UTF-8, ro_RO.UTF-8, ru_RU.UTF-8, sk_SK.UTF-8, sl_SI.UTF-8, sr_YU.UTF-8, sv_SE.UTF-8, tr_TR.UTF-8, uk_UA.UTF-8, zh_CN.UTF-8, zh_HK.UTF-8, zh_TW.UTF-8

To remove above error please export below environment variables
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
'''
os.environ['k2hackbot_development_environment'] = ""
sys_version_info = sys.version_info
python_version = str(sys_version_info[0]) + "." + str(sys_version_info[1]) + "." + str(sys_version_info[2])
if python_version == "3.6.8":
    if "LC_ALL" not in os.environ or os.environ["LC_ALL"] != "en_US.UTF-8":
        console.empty_lines()
        console.print_on_console('Please export following environment variable: export LC_ALL="en_US.UTF-8"')
        sys.exit(0)

console.welcome_greeting_for_user()


def setup_project_config(**kwargs):
    from k2hackbot.config import ConfigureCliParameters
    from k2hackbot.config.config import UserConfig, ProjectConfig
    validate_cli_parameters = ConfigureCliParameters(**kwargs)

    validated_cli_parameters = validate_cli_parameters.validate_configuration()
    config_obj = UserConfig()
    config_obj.set_config_parameters(validated_cli_parameters)

    project_config_obj = ProjectConfig(**kwargs)
    project_config_obj.set_config_parameter("k2_hack_bot_environment", "console")


def install_k2agent():
    console.print_center_text_without_heading("Installing k2agent")
    console.section_breaker()
    from k2hackbot.environment_validation.intcode_enviroment_scan import IntCodeEnvironmentScan

    # console.print_on_console("Step 1.1 Checking Requirements")
    IntCodeEnvironmentScan().check()
    from k2hackbot import k2agent_installer
    k2agent_installer.install_k2_component()
    console.empty_lines()
    #k2agent_installer.verify_application_attachment()


def run_k2crawler():
    console.empty_lines()
    console.print_on_console("Initiating Spider Application Crawling phase", style=console.BOLD)
    from k2hackbot.k2browser import browser_driver
    browser_driver.setup_browser_container()
    from k2hackbot.environment_validation.crawler_environment_scan import CrawlerEnvironmentScan
    from k2hackbot.k2crawler.crawler import setup_and_run_crawler
    console.empty_lines()
    console.print_on_console("Checking requirements for K2Crawler", style=console.BOLD)
    CrawlerEnvironmentScan().check()
    console.empty_lines()
    console.print_on_console("Initiating Application Crawling", style=console.BOLD)
    setup_and_run_crawler()
    console.print_on_console("Application Crawling Done", style=console.BOLD)


def extract_result():
    console.empty_lines()
    console.print_on_console("Analyzing results", style=console.BOLD)
    from k2hackbot.check_vulnerability import verify_and_check_vulnerability_result
    # console.empty_lines()
    # console.print_on_console("Checking Environment for Result Extraction", style=console.BOLD)
    # ResultEnvironmentScan().check()
    verify_and_check_vulnerability_result()


@click.group()
def main():
    pass


@main.command(short_help="""
    K2 Hack Me Tool Verify Given Application is Hackable
""")
@click.option('--applicationLoginUrl', '-apploginurl', type=str, help="Please Provide Login Url for your application ")
@click.option('--applicationIdentifier', '-appid', help="Provide ContainerID/PID of Application Hosted in Docker/Host environment."
                                                        "Provide application Identifier string in given format: {\"containerid\":\"application_container_id\",\"pid\":application_pid}")
@click.option('--isAuthRequired', '-isauthreq', type=bool, default=False, help="If your applications requires authentication using login")
@click.option('--applicationLoginIdentifier', '-apploginid', hidden=True,
              help='''
              If your Application needs a user to be authenticated using login, we need identifier to do that ourself
              Provide identifier  for Application form fields in format as string {"username": {"identification": "user_field_id","value": "user_name"},"password": {"identification": "password_field_id","value": "password"},"submit": {"identification": "submit_button_id","value": "Nothing"}}
              You can find Guide to Do so on following git repository
              https://github.com/k2io/K2ADS
              ''')
@click.option('--k2email', '-k2e', hidden=True, type=str, default="installer@k2io.com", required=True, help='Provide K2 Registered Email')
@click.option('--k2password', '-k2p', hidden=True, type=str, default="123456789a", required=True, help="Provide K2 Registered Email's Password")
@click.option('--remoteMachineCredentials', "-vmcred", hidden=True, help='''Provide Machine details in string format {"ip":"ip", "user":"user", "password":"password"}''')
@click.option('--applicationUrl', '-appurl', multiple=True, callback=convert_to_tuple_from_str, help="Provide Application URls to be crawled")
@click.option('--ignoreUrl', "-ignurl", hidden=True, multiple=True, callback=convert_to_tuple_from_str, help='''Provide Application URLs to be skipped for crawling''')
@click.option('--allowedDomain', "-allowdom", hidden=True, multiple=True, callback=convert_to_tuple_from_str, help='''Application Domains to be used for crawling''')
@click.option('--k2GroupName', "-k2gn", hidden=True, default="IAST", type=str, help='''K2 Group name''')
@click.option('--k2GroupEnv', "-k2ge", hidden=True, default="IAST", type=str, help='''K2 Group deployment Environment''')
@click.option('--k2Cloud', '-k2c', type=str, show_default=True, default="https://k2io.net", help='Provide K2Cloud URL if required')
@click_config_file.configuration_option(implicit=False, provider=json_provider)
def verify_application(**kwargs):
    """
    K2HackBot's Verify Application works as container for K2HackBot's Other separate functionalities
    """
    setup_project_config(**kwargs)
    install_k2agent()
    run_k2crawler()
    extract_result()


if __name__ == '__main__':
    main(obj={})
