import os
import sys
from time import time

import jproperties as jproperties

from k2hackbot.config import UserConfig, project_config, user_config
from k2hackbot.global_classes import HostManager, DockerManager
from k2hackbot.console_handler import console
from k2hackbot.k2manager import K2Manager
import requests
from docker.errors import NotFound as ContainerNotFound


def download_url(url, save_path, chunk_size=128):
    try:
        r = requests.get(url, stream=True, verify=False)
        with open(os.path.join(save_path, "vm-all.zip"), 'wb') as fd:
            for chunk in r.iter_content(chunk_size=chunk_size):
                fd.write(chunk)
    except Exception as e:
        console.print_on_console("Unable to Download K2Agent Installer On the machine Please check")
        sys.exit(0)


class K2Agent(HostManager):
    def __init__(self):
        super().__init__()
        self.k2manger = K2Manager()
        project_config_parameters = project_config.get_config_parameters()
        self.k2agent_version = project_config_parameters["k2_agent_version"]
        self.docker_manager = DockerManager().docker_client
        if project_config_parameters["intcode_host"] == "remote":
            self.installer_path = "/tmp/"

        else:
            self.installer_path = os.path.dirname(__file__) + "/../"

    @console.function_console_wrapper("Downloading K2 Components Installer", "K2 Components Download")
    def download_intcode_installer_zip(self):
        user_config_parameters = user_config.get_config_parameters()
        reference_number = self.k2manger.get_reference_number()[1]
        k2agent_installer_command = """{}/centralmanager/api/v1/help/installers/{}/download/{}/{}/vm-all.zip?isDocker=true&groupName={}&agentDeploymentEnvironment={}&pullPolicyRequired=true
                """.format(self.k2manger.k2cloud, self.k2agent_version, self.k2manger.customer_id, reference_number, user_config_parameters["k2groupname"], user_config_parameters["k2groupenv"])

        # console.print_on_console(k2agent_installer_command)
        download_url(k2agent_installer_command, os.path.dirname(__file__))
        self.extract_intcode_installer_zip()

    @console.function_console_wrapper("Initialising K2Agent ", "K2Agent Initialisation Done")
    def run_intcode_agent(self):
        output = self.execute_on_host(" cd {}k2install;sudo bash k2install.sh -i prevent-web ".format(self.installer_path))
        file_path = os.path.dirname(__file__)
        with open(file_path + "/installer_installation_output.txt", "w+") as f:
            f.write(output)
        if "Can not continue with the installation...exiting due to condition not met" in output and "Congratulations, You've Successfully Installed K2 Segment and K2 Prevent-web Agents" not in output:
            console.stop_printing_animation(result=False)
            console.print_on_console("Failed to Install K2agent on your system. For more logs, please check k2hackbot/k2agent/installer_installation_output.txt")
            sys.exit(0)

    @console.function_console_wrapper("Checking K2Agent's Health ", "K2Agent Running With Proper Health")
    def verify_intcode_health(self):
        try:
            initial_time = time()
            status = False
            while time() - initial_time < 600:
                k2agent_docker_logs = self.docker_manager.get_container_logs("k2agent")
                if "K2-IntCode-Agent entered RUNNING state, process has stayed up for" in k2agent_docker_logs:
                    status = True
                    break
            if not status:
                console.print_on_console("IntCode Start Failure \n ABORTING Automated Scan")
                sys.exit()
        except AttributeError:
            console.print_on_console("k2components not installed correctly")
            sys.exit()

    def check_existing_installer(self):
        try:
            path = os.path.dirname(__file__)
            all_dir_present = os.listdir(path)
        except FileNotFoundError:
            path = "./"
            all_dir_present = os.listdir()

        if 'k2install' in all_dir_present:
            configs = jproperties.Properties()
            with open(os.path.join(path, 'k2install/.agent.properties', 'rb')) as config_file:
                configs.load(config_file)
            k2_version = configs.get("k2_agent_version").data.split("#").strip()
            with open(os.path.join(path, 'k2install/env.properties', 'rb')) as config_file:
                configs.load(config_file)
            customer_id = configs.get("k2_customer_id").data
            if k2_version == self.k2agent_version and customer_id == self.k2manger.customer_id:
                return True
        return False

    def setup_intcode_agent(self):
        k2agent_installation_status = self.check_installed_intcode_agent()
        if not k2agent_installation_status:
            if not self.check_existing_installer():
                self.download_intcode_installer_zip()
            project_config_parameters = project_config.get_config_parameters()
            if project_config_parameters["intcode_host"] == "remote":
                self.paramiko_client.send_file_to_remote()
            self.run_intcode_agent()
            self.verify_intcode_health()
        else:
            console.print_on_console("   > Found K2agent, Skipping K2agent Deployment", style=console.GREEN)

    def verify_application_attachment(self):
        """
        verify container or application attachment in k2agent logs
        :return:
        """

        def prompt_and_validate_applicationidentifier_input(internal_user_config_parameters, retry_str=""):
            from k2hackbot.config import ConfigureCliParameters
            internal_user_config_parameters["applicationidentifier"] = console.get_user_input("Please Provide Container Id/PID of Your Application in required JSON format{}".format(retry_str))
            internal_user_config_parameters = ConfigureCliParameters(**internal_user_config_parameters).validate_application_identifier(validate_at_runtime=True)
            UserConfig().set_config_parameters(internal_user_config_parameters)
            return internal_user_config_parameters

        def verify_pid_existence(internal_application_identifier_json, internal_container_verification_command):
            if "pid" in internal_application_identifier_json.keys():
                if internal_container_verification_command and "containerid" in internal_application_identifier_json.keys():
                    pid_verification_command = int(self.docker_manager.run_on_container("{}".format(internal_application_identifier_json['containerid']), 'sleep 1; ps -o pid= -p {} | wc -l'.format(internal_application_identifier_json['pid']))) >= 1
                elif "containerid" not in internal_application_identifier_json.keys():
                    pid_verification_command = int(self.execute_on_host("ps -o pid= -p {} | wc -l".format(internal_application_identifier_json['pid']))) >= 1
                else:
                    pid_verification_command = False
            else:
                pid_verification_command = True
            return pid_verification_command

        def verify_container_existence(internal_application_identifier_json):
            if "containerid" in application_identifier_json.keys():
                internal_container_verification_command = int(self.execute_on_host("docker ps --no-trunc -q | grep {} | wc -l".format(application_identifier_json['containerid']))) >= 1
            else:
                internal_container_verification_command = True
            return internal_container_verification_command

        try:
            user_config_parameters = user_config.get_config_parameters()
            if user_config_parameters['applicationidentifier'] == "" or user_config_parameters['applicationidentifier'] == {} or user_config_parameters['applicationidentifier'] == None:
                retry_count = 0
                retry_str = ""
                while retry_count < 3:
                    user_config_parameters = prompt_and_validate_applicationidentifier_input(user_config_parameters, retry_str=retry_str)
                    if type(user_config_parameters['applicationidentifier']) is dict:
                        break
                    retry_count += 1
                    if retry_count == 3:
                        console.print_on_console("Max retries count reached. Exiting now.")
                        sys.exit(0)
                    console.print_on_console("Incorrect value provided for applicationIdentifier JSON.")
                    retry_str = " (Retries left {})".format(3 - retry_count)
            application_id = ""
            application_identifier_json = user_config_parameters['applicationidentifier']
            attachment_grep_statement = ""
            if "containerid" in application_identifier_json.keys():
                application_id += application_identifier_json["containerid"]
                attachment_grep_statement += f" | grep -i {application_identifier_json['containerid']}"
            if "pid" in application_identifier_json.keys():
                application_id += str(application_identifier_json["pid"]) if application_id == "" else "(PID " + str(
                    application_identifier_json["pid"]) + ")"
                attachment_grep_statement += f" | grep -i {application_identifier_json['pid']}"
        except Exception as e:
            console.print_on_console("Exception generated during container attachment verification:\n{}".format(e))
            sys.exit(0)
        console.print_with_animation(f"Detecting {application_id} Application", f"Application {application_id} Detected")
        initial_time = time()
        status = False
        container_verification_command = False
        pid_verification_command = False
        try:
            while time() - initial_time < 1600:
                attachment_verification_command = self.docker_manager.run_on_container("k2agent", 'sleep 1; grep -i attachment /tmp/k2logs/k2-intcode_event_status.log {}'.format(attachment_grep_statement))
                if 'Error: No such container: ' in attachment_verification_command:
                    raise ContainerNotFound(message="No such container: k2agent")
                if attachment_verification_command:
                    container_verification_command = verify_container_existence(application_identifier_json)
                    pid_verification_command = verify_pid_existence(application_identifier_json, container_verification_command)
                if attachment_verification_command and container_verification_command and pid_verification_command:
                    console.stop_printing_animation(result=True)
                    status = True
                    break
            if not status:
                console.stop_printing_animation(result=False)
                console.print_on_console(f"Application {application_id} not Detected ")
                sys.exit()
        except ContainerNotFound as e:
            console.stop_printing_animation(result=False)
            console.print_on_console("No such container: k2agent. \nPlease rename K2 Agent container to k2agent.")
            sys.exit(0)

    def check_installed_intcode_agent(self):
        k2agent_process = self.execute_on_host("ps -ef | grep  com.k2cybersecurity.intcodeagent.Runner | wc -l")
        if k2agent_process.strip().isdigit() and int(k2agent_process) > 2:
            return True
        else:
            return False
