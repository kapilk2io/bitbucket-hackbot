from k2hackbot.console_handler import console

from k2hackbot.k2agent import K2Agent

k2_agent = K2Agent()


def verify_application_attachment():
    console.print_on_console("Detecting Application ", style=console.BOLD)
    k2_agent.verify_application_attachment()


def install_k2_component():
    console.empty_lines()
    console.print_on_console("Verifying K2 Components", style=console.BOLD)
    k2_agent.setup_intcode_agent()
