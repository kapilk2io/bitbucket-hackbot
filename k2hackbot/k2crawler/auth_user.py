import sys
from time import sleep, time
import click
# import formasaurus
# from autologin import AutoLogin
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, TimeoutException, NoAlertPresentException, UnexpectedAlertPresentException, NoSuchWindowException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from k2hackbot.global_classes import page_is_loading
from k2hackbot.k2browser import browser_driver
from k2hackbot.console_handler import console
from k2hackbot.k2crawler.application_interaction.alerts import alert_object


class AuthUser:

    def __init__(self):
        self.driver = browser_driver.driver

    # def login_using_auto_login(self, app_url, username, passwordw):
    #     try:
    #         al = AutoLogin()
    #         result = {}
    #         cookies = al.auth_cookies_from_url(app_url, username, passwordw)
    #         for cook in cookies:
    #             for co in cook:
    #                 result[co.name] = co.value
    #         return True, result
    #     except Exception as e:
    #         return False, {}
    #
    # def login_using_formosauraus(self, app_url, username, passwordw):
    #     try:
    #         self.driver.get(app_url)
    #         while not page_is_loading(self.driver):
    #             continue
    #         elem = self.driver.find_element_by_xpath("//*")
    #         source_code = elem.get_attribute("outerHTML")
    #         available_forms_dict = formasaurus.extract_forms(source_code)
    #         if available_forms_dict:
    #             for forms_field_dict in available_forms_dict:
    #                 login_form = forms_field_dict[1]
    #                 if login_form['form'] == "login":
    #                     for field_in_form in login_form['fields']:
    #                         if login_form['fields'][field_in_form] != "password":
    #                             element = self.driver.find_element_by_id(login_form['fields'][field_in_form])
    #                             element.send_keys(username)
    #                         else:
    #                             element = self.driver.find_element_by_id(login_form['fields'][field_in_form])
    #                             element.send_keys(passwordw)
    #                             element.submit()
    #         while not page_is_loading(self.driver):
    #             continue
    #         cookies = self.driver.get_cookies()[0]
    #         return True, cookies
    #     except Exception as e:
    #         return False, {}

    def get_desired_element(self, fieldname, identification):
        try:
            WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//*[@{}]'.format(identification))))
            element = self.driver.find_element_by_xpath('//*[@{}]'.format(identification))
            return element
        except NoSuchElementException as nse:
            console.print_on_console("Found No Element with Given identification for {} {} ".format(fieldname, identification))
            sys.exit(0)
        except TimeoutException as timeout:
            console.print_on_console("Found No Element with Given identification for {} {} ".format(fieldname, identification))
            sys.exit(0)

    def fill_user_name_field(self, identification_dict):
        console.print_with_animation("Filling username field", "Done with UserName ")
        user_name_identification = identification_dict["username"]["identification"]
        user_name_element = self.get_desired_element("UserName", user_name_identification)
        user_name_element.send_keys(identification_dict["username"]['value'])
        console.stop_printing_animation()

    def fill_password_field(self, identification_dict):
        console.print_with_animation("Filling password field", "Done with password  ")
        password_identification = identification_dict["password"]["identification"]
        password_element = self.get_desired_element("Password", password_identification)
        password_element.send_keys(identification_dict["password"]['value'])
        console.stop_printing_animation()

    def submit_login_form(self, identification_dict):
        console.print_with_animation("Submitting Login Form", "Login Form Submission")
        submit_button_element = self.get_desired_element("Submit button", identification_dict["submit"]["identification"])
        submit_button_element.click()
        sleep(2)
        console.stop_printing_animation(result=True)

    def verify_user_login(self, original_cookie):
        console.print_with_animation("Checking User Login", "User Login Status")
        initial_time = time()
        while True:
            if time() - initial_time > 30:
                break
            try:
                after_logged_in_cookie = self.driver.get_cookies()
                if original_cookie != after_logged_in_cookie:
                    console.stop_printing_animation()
                    cookies = {i["name"]: i["value"] for i in after_logged_in_cookie}
                    return True, cookies
                else:
                    console.stop_printing_animation(result=False)
                    return False, after_logged_in_cookie
            except UnexpectedAlertPresentException as uape:
                alert_object.handle_alert()

    def login_using_provided_identifier(self, app_url, form_element_identification, login_identification_type="xpath"):
        try:
            self.driver.get(app_url)
            site_cookie = self.driver.get_cookies()
            while not page_is_loading(self.driver):
                continue
            self.driver.implicitly_wait(90)

            self.fill_user_name_field(form_element_identification)
            self.fill_password_field(form_element_identification)
            self.submit_login_form(form_element_identification)
            return self.verify_user_login(site_cookie)

        except NoSuchElementException as nse:
            click.echo("Found No element {} please check given input".format(nse.__str__()))
            return False, {}
        except NoAlertPresentException as nape:
            click.echo("Found No such alert {}".format(nape.__str__()))
            return False, {}
        except ElementNotInteractableException as enie:
            click.echo("Found element is not intractable {} please check your given input".format(enie.__str__()))
            return False, {}
        except NoSuchWindowException as nswe:
            console.print_on_console("Target browser window is closed. K2HackBot will exit.")
            sys.exit(0)


if __name__ == '__main__':
    # start take values from user
    login_url = "http://192.168.5.86:8081/verademo/login"
    url = "http://192.168.5.86:8081/verademo/"
    user_name = "cody"
    password = "cody"
    user_field_id = 'name="user"'
    password_field_id = 'name="password"'
    submit_button_id = 'name="Login"'
    identification_type = "xpath"
    # driver.get(login_url + "2q4weqrqwerqwr/")
    first_cookies = {}
    auth_user = AuthUser()
    status, cookie = auth_user.login_using_auto_login(login_url, user_name, password)
    if not status:
        print("login using autologin failed")
        status, cookie = auth_user.login_using_formosauraus(login_url, user_name, password)

        if not status:
            print("login using formosouras failed")
            if user_field_id and password_field_id:
                cred_json = {
                    "username": {
                        "identification": user_field_id,
                        "value": user_name
                    },
                    "password": {
                        "identification": password_field_id,
                        "value": password
                    },
                    "submit": {
                        "identification": submit_button_id,
                        "value": "Nothing"
                    }
                }
                status, cookie = auth_user.login_using_provided_identifier(login_url, cred_json)
    for field, value in cookie.items():
        auth_user.driver.add_cookie({"name": field, "value": str(value)})
    print(auth_user.driver.get_cookies())
    sleep(10)
    auth_user.driver.get(url)
    pass
