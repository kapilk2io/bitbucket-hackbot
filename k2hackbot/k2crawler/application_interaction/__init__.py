from k2hackbot.config import user_config


def is_authenticated_run():
    config_prameters = user_config.get_config_parameters()
    if "isauthrequired" in config_prameters and config_prameters["isauthrequired"]:
        if "applicationloginidentifier" in config_prameters:
            return True
    return False


def check_for_username_field(selenium_object):
    if "username" in selenium_object.get_attribute("name") or "email" in selenium_object.get_attribute("name"):
        return True
    return False

def fill_input_in_input_field(selenium_object):
    config_prameters = user_config.get_config_parameters()
    if selenium_object.get_attribute("type") == "password" and is_authenticated_run():
        selenium_object.send_keys(config_prameters["applicationloginidentifier"]["password"]["value"])
    elif is_authenticated_run() and check_for_username_field(selenium_object):
        selenium_object.send_keys(config_prameters["applicationloginidentifier"]["username"]["value"])
    else:
        selenium_object.send_keys("test")
