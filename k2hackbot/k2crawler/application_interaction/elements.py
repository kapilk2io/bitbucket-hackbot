from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support.ui import Select

from k2hackbot.global_classes import console
from k2hackbot.k2browser import browser_driver
from k2hackbot.k2crawler.application_interaction import fill_input_in_input_field


class Element:
    def __init__(self, url, selenium_object):
        self.selenium_object = selenium_object
        self.url = url
        self.tag_name = None
        self.driver = browser_driver.driver
        self.element_location = selenium_object.location
        self.attribute_dict = self.get_element_attributes()
        self.element_call_stack = []
        self.css_class_collections = [
            "input",
            "button",
            "form",
            "select",
            "textarea"
        ]

    # @property
    # def selenium_object(self):
    #     return self.selenium_object
    #
    # @selenium_object.setter
    # def selenium_object(self, new_object):
    #     self.selenium_object = new_object
    #     self.element_location = self.selenium_object.location
    #     self.attribute_dict = self.get_element_attributes()

    def get_element_attributes(self):
        return self.driver.execute_script('var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;', self.selenium_object)

    def relocate(self):
        if self.driver.current_url != self.url:
            self.driver.get(self.url)
        xpath_query = f"//{self.tag_name}"
        arguments = ["@{}='{}'".format(i, j) for i, j in self.attribute_dict.items()]
        whole_query = xpath_query + "[{}]".format(' and '.join(arguments))
        elements = self.driver.find_elements_by_xpath(whole_query)
        for element in elements:
            if element.location == self.element_location:
                self.selenium_object = element
                break

    def interact(self):
        """
        Override this method in subclass
        :return:
        """
        pass


class InputElement(Element):
    def __init__(self, url, selenium_object):
        super().__init__(url, selenium_object)
        self.tag_name = "input"

    def interact(self):
        if self.selenium_object.is_displayed():
            current_value = self.selenium_object.get_attribute("value")
            if current_value is not None:
                current_value = current_value.strip()
            if not current_value:
                fill_input_in_input_field(self.selenium_object)


class ButtonElement(Element):
    def __init__(self, url, selenium_object):
        super().__init__(url, selenium_object)
        self.tag_name = "button"

    def interact(self):
        if self.selenium_object.is_displayed():
            self.selenium_object.click()


class FormElement(Element):
    def __init__(self, url, selenium_object):
        super().__init__(url, selenium_object)
        self.tag_name = "form"

    def interact(self):
        if self.selenium_object.is_displayed():
            form_element = self.selenium_object.find_elements_by_css_selector("*")
            for elements in form_element:
                if elements.tag_name in self.css_class_collections:
                    if elements.tag_name == "input":
                        try:
                            element_type = elements.get_attribute("type")
                            if element_type == "submit":
                                element = ButtonElement(self.url, elements)
                                element.interact()
                                continue
                        except:
                            pass
                        element = InputElement(self.url, elements)
                        element.interact()
                    if elements.tag_name == "textarea":
                        input_element_obj = TextAreaElement(self.url, elements)
                        input_element_obj.interact()
                    if elements.tag_name == "button":
                        input_element_obj = ButtonElement(self.url, elements)
                        input_element_obj.interact()
                    if elements.tag_name == "selection":
                        input_element_obj = SelectionElement(self.url, elements)
                        input_element_obj.interact()

            # input_elements = self.selenium_object.find_elements_by_tag_name("input")
            # for input_element in input_elements:
            #     input_element_obj = InputElement(self.url, input_element)
            #     input_element_obj.interact()
            #
            # input_elements = self.selenium_object.find_elements_by_tag_name("textarea")
            # for input_element in input_elements:
            #     input_element_obj = TextAreaElement(self.url, input_element)
            #     input_element_obj.interact()
            #
            # input_elements = self.selenium_object.find_elements_by_tag_name("button")
            # for input_element in input_elements:
            #     input_element_obj = ButtonElement(self.url, input_element)
            #     input_element_obj.interact()
            #
            # input_elements = self.selenium_object.find_elements_by_tag_name("selection")
            # for input_element in input_elements:
            #     input_element_obj = SelectionElement(self.url, input_element)
            #     input_element_obj.interact()

            # .selenium_object.submit()


class TextAreaElement(Element):
    def __init__(self, url, selenium_object):
        super().__init__(url, selenium_object)
        self.tag_name = "textarea"

    def interact(self):
        if self.selenium_object.is_displayed():
            current_value = self.selenium_object.get_attribute("value")
            if current_value is not None:
                current_value = current_value.strip()
            if not current_value:
                fill_input_in_input_field(self.selenium_object)


class SelectionElement(Element):
    def __init__(self, url, selenium_object):
        super().__init__(url, selenium_object)
        self.tag_name = "selection"

    def interact(self):
        if self.selenium_object.is_displayed():
            select_object = Select(self.selenium_object)
            for option in range(len(select_object.options)):
                try:
                    select_object.select_by_index(option)
                except StaleElementReferenceException:
                    console.print_on_console("Option {} is stale".format(option))


class AnchorElement(Element):
    def __init__(self, url, selenium_object):
        super().__init__(url, selenium_object)
        self.tag_name = "a"

    def interact(self):
        if self.selenium_object.is_displayed():
            pass
