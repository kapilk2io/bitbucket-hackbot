import hashlib

from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, JavascriptException
from k2hackbot.k2browser import browser_driver
from k2hackbot.k2crawler.application_interaction import fill_input_in_input_field
from k2hackbot.k2crawler.application_interaction.elements import InputElement, ButtonElement, FormElement, AnchorElement, SelectionElement, TextAreaElement
from k2hackbot.k2crawler.application_interaction.execution_queue import ExecutionQueue
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException, ElementClickInterceptedException


class Interaction:
    def __init__(self):
        self.known_element_set = set()
        self.driver = browser_driver.driver
        self.execution_queue = ExecutionQueue()
        self.css_class_collections = [
            "input",
            "button",
            "form",
            "select",
            "textarea"
        ]
        '''
        
        '''
        self.element_mapper = {
            "input": InputElement,
            "button": ButtonElement,
            "a": AnchorElement,
            "select": SelectionElement,
            "textarea": TextAreaElement,
            "form": FormElement,
        }

    def check_exists_by_css(self, css):
        try:
            elements = self.driver.find_elements_by_xpath('//{}'.format(css))
        except NoSuchElementException:
            return False
        if elements:
            return True
        return False

    def get_element_by_css(self, css):
        try:
            elements = self.driver.find_elements_by_css_selector(css)
            return elements
        except NoSuchElementException:
            return []

    def get_for_user_input_related_element(self):
        available_element = list(filter(self.check_exists_by_css, self.css_class_collections))
        if available_element:
            return available_element
        else:
            return []

    def interact_with_input_element(self):
        try:
            all_input_element = self.get_element_by_css("input")
            for input_element in all_input_element:
                if not input_element.get_attribute("value"):
                    fill_input_in_input_field(self.selenium_object)
        except ElementNotInteractableException:
            pass

    def interact_with_form_element(self):
        try:
            forms = self.get_element_by_css("form")
            for forms in forms:
                try:
                    forms.submit()
                except StaleElementReferenceException:
                    pass
        except JavascriptException:
            self.driver.find_element_by_css_selector("input[type='submit']").click()

    def get_element_to_interact_web_page(self):
        element_list = []
        elements = self.driver.find_element_by_tag_name("body").find_elements_by_xpath("//*")
        for element in elements:
            # for selenium_obj in self.driver.find_elements_by_tag_name(element):

            if element.tag_name in self.css_class_collections:
                element_obj = self.element_mapper[element.tag_name](self.driver.current_url, element)
                # element_sha = self.get_element_sha(element_obj)
                element_list.append(element_obj)
        return element_list

    def interact_with_web_page(self, web_url):
        if self.driver.current_url != web_url:
            self.driver.get(web_url)
        # available_interact_element = self.get_for_user_input_related_element()
        elements_to_interact = self.get_element_to_interact_web_page()
        for element in elements_to_interact:
            self.execution_queue.append(element)
        self.execute_queue_element()

    def interact_with_href_element(self):
        try:
            for links in self.driver.find_elements_by_xpath("//a[@href]"):
                links.click()
        except ElementNotInteractableException:
            pass

    def interact_with_button_element(self):
        try:
            buttons = self.driver.find_elements_by_xpath("//button")
            for button in buttons:
                if button.is_displayed():
                    button.click()
        except ElementNotInteractableException:
            pass

    def interact_with_element(self, current_elem):
        try:
            current_elem.interact()
        except StaleElementReferenceException:
            current_elem.relocate()
            try:
                current_elem.interact()
            except StaleElementReferenceException:
                return

    def execute_queue_element(self):
        while self.execution_queue:
            try:
                current_elem = self.execution_queue.pop()
                if current_elem.element_call_stack and current_elem.url != self.driver.current_url:
                    for element in current_elem.element_call_stack:
                        self.interact_with_element(element)
                self.interact_with_element(current_elem)

                # if self.driver.current_url != current_elem.url:
                #     interactive_elements = self.get_element_to_interact_web_page()
                #     for new_element in interactive_elements:
                #         new_element.element_call_stack = current_elem.element_call_stack
                #         new_element.element_call_stack.append(current_elem)
                #         self.execution_queue.appendleft(new_element)

            except  ElementClickInterceptedException:
                current_elem.driver.execute_script("arguments[0].click();", current_elem.selenium_object)
                webdriver.ActionChains(current_elem.driver).move_to_element(current_elem.selenium_object).click(current_elem.selenium_object).perform()

    def get_element_sha(self, element):
        element_string = str(element.selenium_object.tag_name + element.selenium_object.get_attribute("innerHTML")).encode()
        hash_string = str(hashlib.sha256(element_string).hexdigest())
        print(element_string, hash_string)
        return
