from selenium.common.exceptions import NoAlertPresentException, ElementNotInteractableException

from k2hackbot.k2browser import browser_driver


class Alert:

    def __init__(self):
        self.driver = browser_driver.driver

    def handle_alert(self):
        try:
            alert_obj = self.driver.switch_to.alert
            alert_obj.accept()
        except ElementNotInteractableException as enie:
            alert_obj.dismiss()
        except NoAlertPresentException as nape:
            self.driver.execute_script("window.alert = function() {};")
            self.driver.execute_script("window.onbeforeunload = function() {};")

alert_object = Alert()