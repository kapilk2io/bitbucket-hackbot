"""This module contains the ``SeleniumMiddleware`` scrapy middleware"""
import sys
from importlib import import_module
from time import time

from scrapy import signals
from scrapy.exceptions import NotConfigured
from scrapy.http import HtmlResponse
from selenium.common.exceptions import UnexpectedAlertPresentException, NoSuchWindowException, WebDriverException
from selenium.webdriver.support.ui import WebDriverWait

from scrapy_selenium.http import SeleniumRequest

from k2hackbot.global_classes import console
from k2hackbot.k2crawler.application_interaction.alerts import alert_object
import os
import psutil


def terminate_current_process():
    current_system_pid = os.getpid()
    this_system = psutil.Process(current_system_pid)
    this_system.terminate()


class SeleniumMiddleware:
    """Scrapy middleware handling the requests using selenium"""

    def __init__(self, driver_name, driver_executable_path, driver_arguments,
                 browser_executable_path, driver_object):
        """Initialize the selenium webdriver

        Parameters
        ----------
        driver_name: str
            The selenium ``WebDriver`` to use
        driver_executable_path: str
            The path of the executable binary of the driver
        driver_arguments: list
            A list of arguments to initialize the driver
        browser_executable_path: str
            The path of the executable binary of the browser
        """

        if not driver_object:
            webdriver_base_path = f'selenium.webdriver.{driver_name}'

            driver_klass_module = import_module(f'{webdriver_base_path}.webdriver')
            driver_klass = getattr(driver_klass_module, 'WebDriver')

            driver_options_module = import_module(f'{webdriver_base_path}.options')
            driver_options_klass = getattr(driver_options_module, 'Options')

            driver_options = driver_options_klass()
            if browser_executable_path:
                driver_options.binary_location = browser_executable_path
            for argument in driver_arguments:
                driver_options.add_argument(argument)

            driver_kwargs = {
                'executable_path': driver_executable_path,
                f'{driver_name}_options': driver_options
            }

            self.driver = driver_klass(**driver_kwargs)
        else:
            driver_module = import_module(driver_object)
            self.driver = getattr(driver_module, 'browser_driver').driver

    @classmethod
    def from_crawler(cls, crawler):
        """Initialize the middleware with the crawler settings"""

        driver_name = crawler.settings.get('SELENIUM_DRIVER_NAME')
        driver_executable_path = crawler.settings.get('SELENIUM_DRIVER_EXECUTABLE_PATH')
        browser_executable_path = crawler.settings.get('SELENIUM_BROWSER_EXECUTABLE_PATH')
        driver_arguments = crawler.settings.get('SELENIUM_DRIVER_ARGUMENTS')
        driver_object = crawler.settings.get('SELENIUM_DRIVER_OBJECT')

        if not driver_name or not driver_executable_path:
            raise NotConfigured(
                'SELENIUM_DRIVER_NAME and SELENIUM_DRIVER_EXECUTABLE_PATH must be set'
            )

        middleware = cls(
            driver_name=driver_name,
            driver_executable_path=driver_executable_path,
            driver_arguments=driver_arguments,
            browser_executable_path=browser_executable_path,
            driver_object=driver_object
        )

        crawler.signals.connect(middleware.spider_closed, signals.spider_closed)

        return middleware

    def process_request(self, request, spider):
        """Process a request using the selenium driver if applicable"""

        if not isinstance(request, SeleniumRequest):
            return None
        try:
            self.driver.get(request.url)
        except WebDriverException as wde:
            console.print_on_console("Chrome not reachable. K2hackbot will exit.")
            terminate_current_process()
        except NoSuchWindowException as nswe:
            console.print_on_console("Target browser window is closed. K2hackbot will exit.")
            terminate_current_process()

        for cookie_name, cookie_value in request.cookies.items():
            # if cookie_name in current_cookies_names:
            #     original_cookie = self.driver.get_cookie(cookie_name)
            #     self.driver.delete_cookie(cookie_name)
            #     original_cookie["value"] = cookie_value
            #     self.driver.add_cookie(original_cookie)
            # else:
            self.driver.add_cookie(
                {
                    'name': cookie_name,
                    'value': cookie_value
                }
            )

        if request.wait_until:
            WebDriverWait(self.driver, request.wait_time).until(
                request.wait_until
            )

        if request.screenshot:
            request.meta['screenshot'] = self.driver.get_screenshot_as_png()

        if request.script:
            self.driver.execute_script(request.script)

        body = ""
        initial_time = time()
        while True:
            if time() - initial_time > 30:
                break
            try:
                page_source = self.driver.page_source
                body = str.encode(page_source)
                break
            except UnexpectedAlertPresentException as uape:
                alert_object.handle_alert()
            except NoSuchWindowException as nswe:
                console.print_on_console("Target browser window is closed. K2hackbot will exit.")
                sys.exit(0)

        # Expose the driver via the "meta" attribute
        request.meta.update({'driver': self.driver})

        return HtmlResponse(
            self.driver.current_url,
            body=body,
            encoding='utf-8',
            request=request
        )

    def spider_closed(self):
        """Shutdown the driver when spider is closed"""

        self.driver.quit()
