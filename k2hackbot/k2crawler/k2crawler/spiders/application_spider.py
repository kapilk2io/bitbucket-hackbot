from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from selenium.common.exceptions import WebDriverException
from tldextract import tldextract
from scrapy_selenium import SeleniumRequest
import re

from k2hackbot.config import user_config
from k2hackbot.global_classes import console
from k2hackbot.k2browser import browser_driver
from k2hackbot.k2crawler.application_interaction.interaction import Interaction


class ApplicationCrawler(CrawlSpider):
    name = 'ApplicationCrawler'

    def __init__(self):
        super().__init__()
        user_config_parameters = user_config.get_config_parameters()
        self.start_urls = user_config_parameters["applicationurl"]
        self.allowed_domains = []
        for i in self.start_urls:
            tsd, td, tsu = tldextract.extract(i)
            if tsu:
                self.allowed_domains.append(td + '.' + tsu)
            else:
                self.allowed_domains.append(td)
        self.rules = [Rule(LinkExtractor(deny=('.*logout.*', '.*reset.*')), callback="parse", follow=True)]
        self.page_interaction = Interaction()
        self.driver = browser_driver.driver
        self.driver_cookies = self.driver.get_cookies()
        self.request_cookies = {}  # eval(os.environ["applicationcookies"])
        self.link_extractor = LinkExtractor()
        self.restricted_urls = ["reset", "logout", "registration"]

        self.complete_url_regex = re.compile(
            r'^(?:http|ftp)s?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)

        self.visited_urls = []
        self.current_url = ""

    def start_requests(self):
        for url in self.start_urls:
            if url:
                self.current_url = url
                try:
                    response = SeleniumRequest(url=url, callback=self.parse)
                    yield response
                except ValueError as e:
                    console.print_on_console("Found following issue with URL {}.\n{}".format(url, e))

    def parse(self, response, **kwargs):
        try:
            console.print_on_console(f"Scanning Url : {response.url} ")
            self.visited_urls.append(response.url)
            self.page_interaction.interact_with_web_page(response.url)
            elems = self.driver.find_elements_by_xpath("//a[@href]")
            all_urls = []
            for elem in elems:
                all_urls.append(elem.get_attribute("href"))
            unique_urls = self.get_unique_links(all_urls)
            for link in unique_urls:
                if link not in self.visited_urls:
                    self.current_url = link
                    yield SeleniumRequest(url=link, cookies=self.request_cookies, callback=self.parse)
        except WebDriverException as e:
            if "ERR_CONNECTION_REFUSED" in e.msg:
                console.print_on_console("Cannot Connect to Application Please check ")

    def is_allowed_url(self, url):
        user_config_parameters = user_config.get_config_parameters()
        for ignore_url_pattern in (self.restricted_urls + user_config_parameters["ignoreurl"]):
            if self.complete_url_regex.match(ignore_url_pattern):
                if ignore_url_pattern == url:
                    self.visited_urls.append(url)
                    console.print_on_console(f"Ignoring url : {url}")
                    return False
            else:
                if re.match(".*{}.*".format(ignore_url_pattern), url):
                    self.visited_urls.append(url)
                    console.print_on_console(f"Ignoring url : {url}")
                    return False
        return True

    def get_unique_links(self, list_of_urls):
        def find_unique(url_collection):
            unique_urls = []
            for url in url_collection:
                if url not in unique_urls and re.match(self.complete_url_regex, url) is not None:
                    if url not in self.visited_urls and self.is_allowed_url(url):
                        unique_urls.append(url)
            return unique_urls

        urls = find_unique(list(filter(None, list_of_urls)))
        return urls
