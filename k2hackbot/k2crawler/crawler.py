from scrapy.utils.project import get_project_settings

from k2hackbot.k2crawler.k2crawler.spiders.application_spider import ApplicationCrawler
from scrapy.crawler import CrawlerProcess


def setup_and_run_crawler():
    crawler_processor = CrawlerProcess(get_project_settings())
    crawler_processor.crawl(ApplicationCrawler)
    crawler_processor.start()

if __name__ == '__main__':
    setup_and_run_crawler()
