#!/usr/bin/python3

import os
import sys

import click
import urllib3
import click_config_file
import json
import copy

from k2hackbot.k2browser import browser_driver

urllib3.disable_warnings()

'''
RuntimeError: Click will abort further execution because Python 3 was configured to use ASCII as encoding for the environment. Consult https://click.palletsprojects.com/python3/ for mitigation steps.
This system lists a couple of UTF-8 supporting locales that you can pick from. The following suitable locales were discovered: af_ZA.UTF-8, am_ET.UTF-8, be_BY.UTF-8, bg_BG.UTF-8, ca_ES.UTF-8, cs_CZ.UTF-8, da_DK.UTF-8, de_AT.UTF-8, de_CH.UTF-8, de_DE.UTF-8, el_GR.UTF-8, en_AU.UTF-8, en_CA.UTF-8, en_GB.UTF-8, en_IE.UTF-8, en_NZ.UTF-8, en_US.UTF-8, es_ES.UTF-8, et_EE.UTF-8, eu_ES.UTF-8, fi_FI.UTF-8, fr_BE.UTF-8, fr_CA.UTF-8, fr_CH.UTF-8, fr_FR.UTF-8, he_IL.UTF-8, hr_HR.UTF-8, hu_HU.UTF-8, hy_AM.UTF-8, is_IS.UTF-8, it_CH.UTF-8, it_IT.UTF-8, ja_JP.UTF-8, kk_KZ.UTF-8, ko_KR.UTF-8, lt_LT.UTF-8, nl_BE.UTF-8, nl_NL.UTF-8, no_NO.UTF-8, pl_PL.UTF-8, pt_BR.UTF-8, pt_PT.UTF-8, ro_RO.UTF-8, ru_RU.UTF-8, sk_SK.UTF-8, sl_SI.UTF-8, sr_YU.UTF-8, sv_SE.UTF-8, tr_TR.UTF-8, uk_UA.UTF-8, zh_CN.UTF-8, zh_HK.UTF-8, zh_TW.UTF-8

To remove above error please export below environment variables
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
'''
# click.disable_unicode_literals_warning = True

from k2hackbot.console_handler import console

os.environ['k2hackbot_development_environment'] = ""
sys_version_info = sys.version_info
python_version = str(sys_version_info[0])+"."+str(sys_version_info[1])+"."+str(sys_version_info[2])
if python_version == "3.6.8":
    if "LC_ALL" not in os.environ or os.environ["LC_ALL"] != "en_US.UTF-8":
        console.empty_lines()
        console.print_on_console('Please export following environment variable: export LC_ALL="en_US.UTF-8"')
        sys.exit(0)


def json_provider(file_path, cmd_name):
    try:
        '''
        Populating default config
        '''
        path = os.path.join(os.path.dirname(__file__), "config/default_config.json")

        with open(path, 'r') as f:
            config_data = f.read()
            config_dict = json.loads(config_data)
    except Exception as e:
        console.print_on_console("Please recheck default config in config/default_config.json. \nFound following issues in default Config file: {}".format(e))
        sys.exit(0)

    try:
        with open(file_path, 'r') as f:
            config_data = f.read()
            # updatings keys in config to lowercase
            user_config_dict = json.loads(config_data)
            user_config_keys = copy.deepcopy(list(user_config_dict.keys()))
            for config_option in user_config_keys:
                user_config_dict[config_option.lower()] = user_config_dict.pop(config_option)

        for config_option in user_config_dict.keys():
            config_dict[config_option] = user_config_dict[config_option]
        return config_dict
    except Exception as e:
        console.print_on_console("Please recheck config in {}. \nFound following issues in Config file: {}".format(file_path, e))
        sys.exit(0)

def convert_to_tuple_from_str(ctx, param, value):
    try:
        if type(value) == str:
            return [value]
        else:
            return list(value)
    except Exception as e:
        raise click.BadParameter(f'Unable to convert %s parameter to tuple' % (param))


@click.group()
def main():
    pass


@main.command(short_help="Install K2agent")
# @add_options(default_options)
@click.option('--k2email', '-k2e', hidden=True, type=str, default="installer@k2io.com", required=True, help='Provide K2 Registered Email')
@click.option('--k2password', '-k2p', hidden=True, type=str, default="123456789a", required=True, help="Provide K2 Registered Email's Password")
@click.option('--applicationIdentifier', '-appid', help="Provide ContainerID/PID of Application Hosted in Docker/Host environment."
                                                                                 "Provide application Identifier string in given format: {\"containerid\":\"application_container_id\",\"pid\":application_pid}")
@click.option('--remoteMachineCredentials', "-vmcred", hidden=True, help='''Provide Machine details in string format {"ip":"ip", "user":"user", "password":"password"}''')
@click.option('--k2GroupName', "-k2gn", hidden=True, default="IAST", type=str, help='''K2 Group name''')
@click.option('--k2GroupEnv', "-k2ge", hidden=True, default="IAST", type=str, help='''K2 Group deployment Environment''')
@click.option('--k2Cloud', '-k2c', type=str, show_default=True, default="https://k2io.net", help='Provide K2cloud URL if required')
@click_config_file.configuration_option(implicit=False, provider=json_provider)
def install_k2agent(**kwargs):
    console.welcome_greeting_for_user()
    from k2hackbot.config import ConfigureJenkinsParameters
    from k2hackbot.config.config import UserConfig, ProjectConfig
    validate_cli_parameters = ConfigureJenkinsParameters(**kwargs)
    validated_cli_parameters = validate_cli_parameters.validate_install_agent_parameters()
    user_config_obj = UserConfig()
    user_config_obj.set_config_parameters(validated_cli_parameters)
    kwargs = user_config_obj.get_config_parameters()

    project_config_obj = ProjectConfig(**kwargs)
    project_config_obj.set_config_parameter("k2_hack_bot_environment","jenkins")

    console.print_center_text_without_heading("Installing k2agent on system")
    console.section_breaker()
    from k2hackbot.environment_validation.intcode_enviroment_scan import IntCodeEnvironmentScan

    IntCodeEnvironmentScan().check()
    from k2hackbot import k2agent_installer
    k2agent_installer.install_k2_component()
    console.empty_lines()
    k2agent_installer.verify_application_attachment()


# @k2hackbot.group(short_help="Start K2 Crawler")
# def k2crawler(**kwargs):
#     console.print_center_text_without_heading("Step 2. Setting Up K2Crawler ")
#     console.print_decorated_heading("We are setting Up Crawler for Application Detection")
#     pass


@main.command(short_help="Check Results of ADS")
@click.option('--k2email', '-k2e', hidden=True, type=str, default="installer@k2io.com", required=True, help='Provide K2 Registered Email')
@click.option('--k2password', '-k2p', hidden=True, type=str, default="123456789a", required=True, help="Provide K2 Registered Email's Password")
@click.option('--remoteMachineCredentials', "-vmcred", hidden=True, help='''Provide Machine details in string format {"ip":"ip", "user":"user", "password":"password"}''')
@click.option('--applicationIdentifier', '-appid', required=True, help="Provide ContainerID/PID of Application Hosted in Docker/Host environment."
                                                                                 "Provide application Identifier string in given format: {\"containerid\":\"application_container_id\",\"pid\":application_pid}")
@click.option('--k2Cloud', '-k2c', type=str, show_default=True, default="https://k2io.net", help='Provide K2Cloud URL if required')
@click_config_file.configuration_option(implicit=False, provider=json_provider)
def extract_result(**kwargs):
    console.empty_lines()
    console.print_on_console("Analyzing results", style=console.BOLD)

    from k2hackbot.config import ConfigureJenkinsParameters
    from k2hackbot.config.config import UserConfig, ProjectConfig
    validate_cli_parameters = ConfigureJenkinsParameters(**kwargs)
    validated_cli_parameters = validate_cli_parameters.validate_extract_result_parameters()
    config_obj = UserConfig()
    config_obj.set_config_parameters(validated_cli_parameters)
    kwargs = config_obj.get_config_parameters()

    project_config_obj = ProjectConfig(**kwargs)
    project_config_obj.set_config_parameter("k2_hack_bot_environment","jenkins")

    from k2hackbot.check_vulnerability import verify_and_check_vulnerability_result
    # console.print_on_console("Checking Environment for Result Extraction", style=console.BOLD)
    # ResultEnvironmentScan().check()
    verify_and_check_vulnerability_result()


# @k2crawler.group('auth')
# def auth():
#     pass


@main.command(short_help="Crawl Your application using K2Crawler")
@click.option('--applicationUrl', '-appurl', multiple=True, callback=convert_to_tuple_from_str, help="Provide Application URls to be crawled")
@click.option('--ignoreUrl', "-ignurl", hidden=True, multiple=True, callback=convert_to_tuple_from_str, help='''Provide Application URLs to be skipped for crawling''')
@click.option('--allowedDomain', "-allowdom", hidden=True, multiple=True, callback=convert_to_tuple_from_str, help='''Application Domains to be used for crawling''')
@click.option('--isAuthRequired', '-isauthreq', type=bool, default=False, help="If your applications requires authentication using login")
@click.option('--applicationLoginUrl', '-apploginurl', type=str, help="Please Provide Login Url for your application ")
@click.option('--applicationLoginIdentifier', '-apploginid', hidden=True,
              help='''
              If your Application needed user to be authenticated using login, we need identifier to do that ourself
              Provide identifier  for Application form fields in format as string {"username": {"identification": "user_field_id","value": "user_name"},"password": {"identification": "password_field_id","value": "password"},"submit": {"identification": "submit_button_id","value": "Nothing"}}
              You can find Guide to Do so on following git repository
              https://github.com/k2io/K2ADS
              ''')
@click.option('--remoteMachineCredentials', "-vmcred", hidden=True, help='''Provide Machine details in string format {"ip":"ip", "user":"user", "password":"password"}''')
@click_config_file.configuration_option(implicit=False, provider=json_provider)
def crawl_application(**kwargs):
    console.empty_lines()
    console.print_on_console("Initiating Spider Application Crawling phase", style=console.BOLD)
    # console.print_decorated_heading("We are setting Up Crawler for Application Detection")
    # os.environ["isauthrequired"] = "True" if kwargs["isauthrequired"] else "False"
    from k2hackbot.config import ConfigureJenkinsParameters
    from k2hackbot.config.config import UserConfig, ProjectConfig
    validate_cli_parameters = ConfigureJenkinsParameters(**kwargs)
    validated_cli_parameters = validate_cli_parameters.validate_crawl_application_parameters()
    config_obj = UserConfig()
    config_obj.set_config_parameters(validated_cli_parameters)
    kwargs = config_obj.get_config_parameters()

    project_config_obj = ProjectConfig(**kwargs)
    project_config_obj.set_config_parameter("k2_hack_bot_environment","jenkins")

    from k2hackbot.k2browser import browser_driver
    browser_driver.setup_browser_container()
    from k2hackbot.environment_validation.crawler_environment_scan import CrawlerEnvironmentScan
    from k2hackbot.k2crawler.crawler import setup_and_run_crawler

    console.empty_lines()
    console.print_on_console("Checking requirements for K2Crawler", style=console.BOLD)
    CrawlerEnvironmentScan().check()

    console.empty_lines()
    console.print_on_console("Initiating Application Crawling", style=console.BOLD)
    setup_and_run_crawler()
    console.print_on_console("Application Crawling Done", style=console.BOLD)


# @k2crawler.command(short_help="ADS for web application which requires authentication")
# @click.option('--applicationUrl', '-appurl', required=True, default=["http://192.168.5.86:8080/DemoApplication-0.0.1-SNAPSHOT/"], multiple=True, callback=convert_to_tuple_from_str, help="Provide Application URls to be crawled")
# @click.option('--applicationLoginIdentifier', '-apploginid', hidden=True,
#               help='''
#               If your Application needed user to be authenticated using login, we need identifier to do that ourself
#               Provide identifier  for Application form fields in format as string {"username": {"identification": "user_field_id","value": "user_name"},"password": {"identification": "password_field_id","value": "password"},"submit": {"identification": "submit_button_id","value": "Nothing"}}
#               You can find Guide to Do so on following git repository
#               https://github.com/k2io/K2ADS
#               ''')
# @click.option('--remoteMachineCredentials', "-vmcred", help='''Provide Machine details in string format {"ip":"ip", "user":"user", "password":"password"}''')
# def with_auth(**kwargs):
#     from k2hackbot.global_classes import ConfigureCrawlerEnvironmentVariable
#     os.environ["isauthrequired"] = "True"
#     console.empty_lines()
#     console.print_on_console("Initiating Spider Application Crawling phase", style=console.BOLD)
#     # console.print_decorated_heading("We are setting Up Crawler for Application Detection")
#     os.environ["isauthrequired"] = "True" if kwargs["isauthrequired"] else "False"
#
#     from k2hackbot.global_classes import ConfigureCrawlerEnvironmentVariable
#     ConfigureCrawlerEnvironmentVariable(**kwargs)
#     from k2hackbot.environment_validation.crawler_environment_scan import CrawlerEnvironmentScan
#     from k2hackbot.k2crawler.crawler import setup_and_run_crawler
#     console.empty_lines()
#     console.print_on_console("Checking requirements for K2Crawler", style=console.BOLD)
#     CrawlerEnvironmentScan().check()
#
#     console.print_on_console("Initiating Application Crawling", style=console.BOLD)
#     setup_and_run_crawler()
#     console.empty_lines()
#     console.print_on_console("Application Crawling Done", style=console.BOLD)


if __name__ == '__main__':
    main(obj={})
