# Todo Update K2manager file for k2manager methods
import sys
from datetime import timezone, datetime, date
import click
import requests
import calendar
from k2hackbot.config import user_config, project_config
from k2hackbot.console_handler import console
from requests import get, post, put, delete


class K2Manager:
    def __init__(self):
        project_config_parameters = project_config.get_config_parameters()
        user_config_parameters = user_config.get_config_parameters()
        username = user_config_parameters['k2email'] if "k2email" in user_config_parameters else "installer@k2io.com"
        password = user_config_parameters['k2password'] if "k2password" in user_config_parameters else "123456789a"
        self.password = password
        self.username = username
        self.json_header = {
            'cache-control': 'no-cache',
            'content-type': 'application/x-www-form-urlencoded',
        }
        self.customer_credential = [
            ('j_username', '{}'.format(self.username)),
            ('j_password', '{}'.format(self.password)),
        ]
        self.k2cloud = project_config_parameters["k2cloud"]
        self.customer_login_uri = '{}/centralmanager/login'.format(self.k2cloud)
        self.cookies = self.create_customer_session()[2]
        self.customer_id = self.cookies.get_dict()['customerId']

    def _get(self, url):
        return requests.get(url)

    def _post(self, url, *args, **kwargs):
        return requests.post(url, **kwargs)

    def check_user(self):
        pass

    def k2_api_request(self, uri, headers, body, cookies, method="post"):
        methods = {"get": get, "post": post, "put": put, "delete": delete}
        url = self.k2cloud + uri

        try:
            response = methods[method](url, json=body, headers=headers, cookies=cookies,
                                       verify=False)
            # click.echo("K2 API { %s } Status: %s" % (uri, response.status_code))
            # click.echo("K2 API { %s } Response: %s" % (uri, response.content))
            if response.status_code // 100 == 2:
                return [True, response]
            else:
                return [False, response]
        except requests.RequestException as e:
            click.echo("%s" % e)
            return [False, e]

    def get_todays_timestamp(self):
        timestamp1 = calendar.timegm(date.today().timetuple())
        dt = datetime.utcfromtimestamp(timestamp1)
        timestamp = int(dt.replace(tzinfo=timezone.utc).timestamp()) * 1000
        return timestamp

    def create_customer_session(self):
        query_response = self._post(self.customer_login_uri, headers=self.json_header, data=self.customer_credential,
                                    verify=False)
        if query_response.status_code != 200:
            console.print_on_console("Given K2m User {} is not verified at {}. Please check K2Cloud and User Credentials.".format(self.username, self.k2cloud))
            sys.exit(0)
        else:
            self.cookies = query_response.cookies
            return [True, 200, query_response.cookies]

    def get_reference_number(self):
        api_uri = "/centralmanager/api/v1/help/installers/reference/{}".format(self.customer_id)
        body = {}
        headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
        }

        response = self.k2_api_request(api_uri, headers, body, self.cookies, method="get")
        if response[0]:
            data = response[1].json()['referenceToken']
            # logger.debug("All Attack Counts: {}".format(incident_counts))
            return True, data
        else:
            return False, "Failed"

    def get_policy_info(self, policy_type, group_name):
        """
        Get policy info from k2m of the user
        URL: centralmanager/api/v1/process/:USERID
        :param cookies:
        :return:
        """
        api_uri = "/centralmanager/api/v1/policy/ic/config/{}?group={}&policy={}".format(self.customer_id, group_name,
                                                                                         policy_type)
        body = {"draw": 1, "pageNumber": 1, "recordsPerPage": 200000, "startTime": self.get_todays_timestamp()}
        headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
        }

        response = self.k2_api_request(api_uri, headers, body, self.cookies, method="get")
        if response[0]:
            data = response[1].json()['data']
            # logger.debug("All Attack Counts: {}".format(incident_counts))
            return True, data
        else:
            return False, "Failed"

    def update_user_policy(self, policy_type, policy, group_name):
        """
        Following method will call k2m api to get incident count for given containers
        URL: /api/v1/ic/incident/:USERID/:EVENT_TYPE
        :param container_ids:
        :return:
        """

        uri = "/centralmanager/api/v1/policy/ic/agent/config/{}?policyType={}".format(self.customer_id, policy_type)

        body = {
            'policy': policy,
            'groupName': group_name
        }
        headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
        }

        response = self.k2_api_request(uri, headers, body, cookies=self.cookies, method="put")

        if str(response[1].content):
            # logger.debug("All Attack Counts: {}".format(incident_counts))
            return True, "Successful"
        else:
            return False, "Failed"

    def update_k2m_policy(self):
        click.echo('Updating agent policy: IAST')
        # Update Agent Policy
        get_policy_status, user_current_policy = self.get_policy_info(policy_type='agentPolicy',
                                                                      group_name='IAST')

        click.echo(f'user_current_policy --> {user_current_policy}')

        user_policy = user_current_policy
        user_policy["protectionMode"]["enabled"] = False
        user_policy["iastMode"]["staticScanning"]["enabled"] = True
        user_policy["iastMode"]["dynamicScanning"]["enabled"] = True

        status, _ = self.update_user_policy(policy_type="agentPolicy", policy={"agentPolicy": user_policy},
                                            group_name='IAST')
        if status:
            click.echo('Agent policy updated successfully')
            return status
        else:
            click.echo('ABORTING TEST: Cannot update Agent policy')
            return status

    def get_latest_intcode_verion(self):
        pass

    def check_connection(self):
        try:
            response = self._get(self.k2cloud)
            return response
        except:
            return False

    def get_group_deployed_env_info(self):
        """
        Get policy deployed env info from k2m of the user
        URL: centralmanager/api/v1/policy/ic/group/paginated/:USERID
        :param cookies:
        :return:
        """
        api_uri = "/centralmanager/api/v1/policy/ic/group/paginated/{}".format(self.customer_id)
        body = {"draw": 1, "pageNumber": 1, "recordsPerPage": 200000, "startTime": self.get_todays_timestamp()}
        headers = {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
        }

        response = self.k2_api_request(api_uri, headers, body, self.cookies, method="post")
        if response[0]:
            data = response[1].json()['data']
            # logger.debug("All Attack Counts: {}".format(incident_counts))
            return True, data
        else:
            return False, "Failed"
