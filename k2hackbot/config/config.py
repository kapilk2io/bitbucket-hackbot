import json
import os
import sys

from k2hackbot.k2logger import logger


class UserConfig(object):
    """docstring for UserConfig."""

    config_parameters = None

    def __init__(self):
        super(UserConfig, self).__init__()
        try:
            path = os.path.join(os.path.dirname(__file__), "user_config.json")
            with open(path) as file:
                UserConfig.config_parameters = json.load(file)
        except Exception as e:
            print("Following exception generated during reading user config from user_config.json file:\n{}".format(e))
            sys.exit(0)

    def get_config_parameters(self):
        return UserConfig.config_parameters

    def set_config_parameters(self, cli_parameters):
        try:
            path = os.path.join(os.path.dirname(__file__), "user_config.json")
            with open(path, "w+") as file:
                json.dump(cli_parameters, file, indent=4)
            UserConfig.config_parameters = cli_parameters
        except Exception as e:
            print("Following exception generated during writing user config to user_config.json file:\n{}".format(e))

    def get_config_parameter(self, name):
        if name in UserConfig.config_parameters:
            return UserConfig.config_parameters[name]
        else:
            return None


class ProjectConfig(object):
    """docstring for ProjectConfig."""

    config_parameters = None

    def set_intcode_host(self, user_config_parameters):
        if "remotemachinecredentials" in user_config_parameters and user_config_parameters["remotemachinecredentials"] != "" and user_config_parameters["remotemachinecredentials"] != {} and user_config_parameters['remotemachinecredentials'] != None:
            ProjectConfig.config_parameters["intcode_host"] = "remote"
        else:
            ProjectConfig.config_parameters["intcode_host"] = "local"

    def set_chromedriver(self, user_config_parameters):
        if "remotemachinecredentials" in user_config_parameters and user_config_parameters["remotemachinecredentials"] != "" and user_config_parameters["remotemachinecredentials"] != {} and user_config_parameters['remotemachinecredentials'] != None:
            remote_machine_ip, _, _ = user_config_parameters["remotemachinecredentials"].values()
            ProjectConfig.config_parameters["chromedriver"] = 'http://{}:4444/wd/hub'.format(remote_machine_ip)
        else:
            ProjectConfig.config_parameters["chromedriver"] = 'http://localhost:4444/wd/hub'

    def set_k2cloud(self, user_config_parameters):
        if "k2cloud" in user_config_parameters:
            ProjectConfig.config_parameters['k2cloud'] = user_config_parameters['k2cloud']

    def __init__(self, **kwargs):
        super(ProjectConfig, self).__init__()
        user_config_parameters = UserConfig().get_config_parameters()
        if "k2hackbot_development_environment" in os.environ.keys() and os.environ['k2hackbot_development_environment'].lower() == "development":
            project_config_file = "development_level_project_config.json"
        else:
            project_config_file = "project_config.json"
        try:
            path = os.path.join(os.path.dirname(__file__), project_config_file)
            with open(path) as file:
                ProjectConfig.config_parameters = json.load(file)
        except Exception as e:
            print("Following exception generated during reading project config from {} file:\n{}".format(project_config_file, e))
            sys.exit(0)

        # this environment variable is used by get_project_settings function of scrapy package in k2crawler/crawler.py
        os.environ['SCRAPY_SETTINGS_MODULE'] = ProjectConfig.config_parameters["SCRAPY_SETTINGS_MODULE"]
        os.environ['WDM_LOG_LEVEL'] = ProjectConfig.config_parameters["WDM_LOG_LEVEL"]  # silent logs generated during starting chrome browser for crawling

        self.set_intcode_host(user_config_parameters)
        self.set_chromedriver(user_config_parameters)
        self.set_k2cloud(user_config_parameters)

    def get_config_parameters(self):
        return ProjectConfig.config_parameters

    def get_config_parameter(self, name):
        if name in ProjectConfig.config_parameters:
            return ProjectConfig.config_parameters[name]
        else:
            return None

    def set_config_parameter(self, name, value):
        ProjectConfig.config_parameters[name] = value
