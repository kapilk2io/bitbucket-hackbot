import sys
import json

from k2hackbot.k2logger import logger
from k2hackbot.config.config import UserConfig, ProjectConfig
from k2hackbot.console_handler import console
import re


def display_config_valiadation_failed_message(msg):
    final_msg = msg + "\n\nFor more help, please use --help option"
    console.print_on_console(final_msg)
    sys.exit(0)

class ConfigureCliParameters:
    """docstring for ConfigureCliParameters."""

    def __init__(self, **kwargs):
        self.config_parameters = kwargs

    def set_application_url(self):
        self.config_parameters["applicationurl"] = list((self.config_parameters["applicationloginurl"],))

    def validate_application_url(self):
        # validates the URLs provided by User to be crawled
        def application_url_update_required(config_params):
            '''
                checks whether the application login provided needs any update
                returns True when application Url is not provided in authenticated k2hackbot run by user and applicationLoginUrl is provided
            '''
            application_url_update_required_status = config_params["isauthrequired"] and config_params[
                "applicationloginurl"] != "" and (len(config_params["applicationurl"]) == 0 or (
                        len(config_params["applicationurl"]) == 1 and config_params["applicationurl"][0] == ""))
            return application_url_update_required_status

        if application_url_update_required(self.config_parameters):
            self.set_application_url()
        elif "" in self.config_parameters["applicationurl"]:
            display_config_valiadation_failed_message("Please provide valid input for applicationUrl:\nSample Input: ['https://www.example.com/home'] ")
        elif len(self.config_parameters["applicationurl"]) == 0:
            display_config_valiadation_failed_message("Please provide input for applicationUrl:\nSample Input: ['https://www.example.com/home'] ")

    def validate_application_identifier(self, validate_at_runtime=False):
        # Validates the applicationidenitfier JSON provided by user
        try:
            if type(self.config_parameters['applicationidentifier']) is dict:
                self.config_parameters['applicationidentifier'] = json.dumps(self.config_parameters['applicationidentifier'])
            self.config_parameters['applicationidentifier'] = json.loads(self.config_parameters['applicationidentifier'])
            application_identifier_json = self.config_parameters['applicationidentifier']

            if type(application_identifier_json) != dict or ("containerid" not in application_identifier_json.keys() and "pid" not in application_identifier_json.keys()):
                if not validate_at_runtime:
                    display_config_valiadation_failed_message("Please provide valid input for applicationIdentifier:\nSample Input: {\"containerid\":\"application_container_id\",\"pid\":application_pid} ")
            return self.config_parameters

        except ValueError as e:
            if not validate_at_runtime:
                display_config_valiadation_failed_message("Please provide valid correct JSON for applicationIdentifier. Following error occurred during parsing the applicationIdentifier json:\n{} ".format(e))
            return self.config_parameters

    def validate_ignore_url(self):
        # Validates the list of URL provided by user in ignoreUrl parameter
        ignore_url = self.config_parameters['ignoreurl']
        if "" in ignore_url:
            display_config_valiadation_failed_message("Please provide valid input for ignoreUrl in prescribed format.\nSample Input: ['https://www.example.com/home'] ")

    def validate_allowed_domain(self):
        # Validates the list of domains provided by user in allowedDomain parameter
        allowed_domain = self.config_parameters['alloweddomain']
        if "" in allowed_domain:
            display_config_valiadation_failed_message("Please provide valid input for allowedDomain.\nSample Input: ['example.com'] ")

    def validate_k2email(self):
        # Validates the k2email parameter provided in user configuration
        k2email = self.config_parameters['k2email']
        if k2email == "" or type(k2email) != str:
            display_config_valiadation_failed_message("Please provide valid input for k2email.\nSample Input: 'abc@k2io.com' ")

    def validate_k2password(self):
        # Validates the k2password parameter provided in user configuration
        k2password = self.config_parameters['k2password']
        if k2password == "" or type(k2password) != str:
            display_config_valiadation_failed_message("Please provide valid input for k2password.\nSample Input: 'password' ")

    def validate_application_login_url(self):
        # validates the applicationloginurl config parameter provided by user
        def application_login_url_update_required(config_params):
            '''
            checks whether the application login url provided needs any update
            returns True when application login Url is not provided in authenticated k2hackbot run by user and number of URls in applicationURL is 1
            '''
            application_login_url_update_required_status = (config_params["applicationloginurl"] == "" or config_params[
                "applicationloginurl"] == None or type(config_params["applicationloginurl"]) != str) and len(
                config_params["applicationurl"]) == 1
            return application_login_url_update_required_status

        if application_login_url_update_required(self.config_parameters):
            self.config_parameters['applicationloginurl'] = self.config_parameters["applicationurl"][0]
        application_login_url = self.config_parameters['applicationloginurl']
        if application_login_url == "":
            display_config_valiadation_failed_message("Please provide login url in applicationLoginUrl.\nSample Input: 'https://www.example.com/login' ")
        if type(application_login_url) != str:
            display_config_valiadation_failed_message("Please provide valid login url in applicationLoginUrl.\nSample Input: 'https://www.example.com/login' ")

    def validate_application_login_identifier(self):
        # Validated the applicationloginidenitifer json provided by user
        def validate_application_login_identifier_key(key, login_cred_json):
            # Validates the input value and keys provided by User in login Idenitifer JSON
            if login_cred_json == {}:
                display_config_valiadation_failed_message("""Please provide input for applicationloginIdentifier\nSample Input: "{\"username\": {\"identification\": \"user_field_id\",\"value\": \"user_field_value\"},\"password\": {\"identification\": \"password_field_id\",\"value\": \"password_field_value\"},\"submit\": {\"identification\": \"submit_button_id\",\"value\": \"submit_button_value\"}}" """)
            if key not in login_cred_json.keys():
                display_config_valiadation_failed_message("""Please provide {} key in applicationloginIdentifier\nSample Input: "{{\"username\": {{\"identification\": \"user_field_id\",\"value\": \"user_field_value\"}},\"password\": {{\"identification\": \"password_field_id\",\"value\": \"password_field_value\"}},\"submit\": {{\"identification\": \"submit_button_id\",\"value\": \"submit_button_value\"}}}}" """.format(key))

            if "identification" not in login_cred_json[key].keys():
                display_config_valiadation_failed_message("""Please provide identification key input for {} in applicationloginIdentifier\nSample Input: "{{\"username\": {{\"identification\": \"user_field_id\",\"value\": \"user_field_value\"}},\"password\": {{\"identification\": \"password_field_id\",\"value\": \"password_field_value\"}},\"submit\": {{\"identification\": \"submit_button_id\",\"value\": \"submit_button_value\"}}}}" """.format(key))
            elif login_cred_json[key]["identification"] == "":
                display_config_valiadation_failed_message("""Please provide valid identification key input for {} in applicationloginIdentifier\nSample Input: "{{\"username\": {{\"identification\": \"user_field_id\",\"value\": \"user_field_value\"}},\"password\": {{\"identification\": \"password_field_id\",\"value\": \"password_field_value\"}},\"submit\": {{\"identification\": \"submit_button_id\",\"value\": \"submit_button_value\"}}}}" """.format(key))
            if "value" not in login_cred_json[key].keys():
                display_config_valiadation_failed_message("Please provide value key input for {} in applicationloginIdentifier ".format(key))
            elif login_cred_json[key]["value"] == "":
                display_config_valiadation_failed_message("""Please provide valid value key input for {} in applicationloginIdentifier\nSample Input: "{{\"username\": {{\"identification\": \"user_field_id\",\"value\": \"user_field_value\"}},\"password\": {{\"identification\": \"password_field_id\",\"value\": \"password_field_value\"}},\"submit\": {{\"identification\": \"submit_button_id\",\"value\": \"submit_button_value\"}}}}" """.format(key))
            validate_application_login_identifier_key_status = key not in login_cred_json.keys() or (
                        "identification" not in login_cred_json[key].keys() or login_cred_json[key][
                    "identification"] == "") or ("value" not in login_cred_json[key].keys() or login_cred_json[key][
                "value"] == "")
            return validate_application_login_identifier_key_status

        try:
            application_login_identifier = self.config_parameters['applicationloginidentifier']
            if type(self.config_parameters['applicationloginidentifier']) is dict:
                self.config_parameters['applicationloginidentifier'] = json.dumps(self.config_parameters['applicationloginidentifier'])

            self.config_parameters['applicationloginidentifier'] = json.loads(self.config_parameters['applicationloginidentifier'])
            login_cred_json = self.config_parameters['applicationloginidentifier']
            validate_application_login_identifier_key("username", login_cred_json)
            validate_application_login_identifier_key("password", login_cred_json)
            validate_application_login_identifier_key("submit", login_cred_json)
        except ValueError as e:
            display_config_valiadation_failed_message("Please recheck the applicationLoginIdentifier JSON. Following error occurred during parsing the applicationLoginIdentifier json:\n{} ".format(e))
        except Exception as e:
            display_config_valiadation_failed_message("""Please recheck the applicationLoginIdentifier JSON. %s\nSample Input: "{\"username\": {\"identification\": \"user_field_id\",\"value\": \"user_field_value\"},\"password\": {\"identification\": \"password_field_id\",\"value\": \"password_field_value\"},\"submit\": {\"identification\": \"submit_button_id\",\"value\": \"submit_button_value\"}}" """)

    def validate_remote_machine_credentials(self):
        #validates the remotemachinecredentials json provided by user
        remote_machine_credentials = self.config_parameters['remotemachinecredentials']
        try:
            if type(self.config_parameters['remotemachinecredentials']) is dict:
                self.config_parameters['remotemachinecredentials'] = json.dumps(self.config_parameters['remotemachinecredentials'])
            remote_machine_credentials = self.config_parameters['remotemachinecredentials']
            self.config_parameters['remotemachinecredentials'] = json.loads(self.config_parameters['remotemachinecredentials'])
            remote_cred_json = self.config_parameters['remotemachinecredentials']
            remote_cred_json_keys = remote_cred_json.keys()
            if "ip" not in remote_cred_json_keys or "user" not in remote_cred_json_keys or "password" not in remote_cred_json_keys:
                display_config_valiadation_failed_message("""Please recheck the input for remoteMachineCredentials. \nSample Input: "{\"ip\":\"192.16.0.1\", \"user\":\"my_user\", \"password\":\"my_password\"}" """)
            if "ip" in remote_cred_json_keys and remote_cred_json['ip'] == "":
                display_config_valiadation_failed_message("""Please recheck the input provided for ip in remoteMachineCredentials. \nSample Input: "{\"ip\":\"192.16.0.1\", \"user\":\"my_user\", \"password\":\"my_password\"}" """)
            if "user" in remote_cred_json_keys and remote_cred_json['user'] == "":
                display_config_valiadation_failed_message("""Please recheck the input provided for user in remoteMachineCredentials. \nSample Input: "{\"ip\":\"192.16.0.1\", \"user\":\"my_user\", \"password\":\"my_password\"}" """)
            if "password" in remote_cred_json_keys and remote_cred_json['password'] == "":
                display_config_valiadation_failed_message("""Please recheck the input provided for password in remoteMachineCredentials. \nSample Input: "{\"ip\":\"192.16.0.1\", \"user\":\"my_user\", \"password\":\"my_password\"}" """)
        except ValueError as e:
            display_config_valiadation_failed_message("Please recheck the remoteMachineCredentials JSON. \nFollowing error occurred during parsing the remoteMachineCredentials json:\n{} ".format(e))
        except Exception as e:
            display_config_valiadation_failed_message("""Please recheck the remoteMachineCredentials JSON. \nSample Input: "{\"ip\":\"192.16.0.1\", \"user\":\"my_user\", \"password\":\"my_password\"}" """)

    def validate_k2cloud(self):
        # Validates the k2cloud option provided by user
        pattern = "^https?:\/\/[a-zA-Z0-9.]+$"
        cloud_str = self.config_parameters["k2cloud"]
        result = re.match(pattern, cloud_str)
        if not result:
            display_config_valiadation_failed_message("""Please recheck the k2Cloud string. \nSample Input: "https://www.k2io.net" """)

    def validate_configuration(self):
        # Validates the user configuration for command line options
        self.validate_application_url()
        self.validate_ignore_url()
        if not (self.config_parameters['applicationidentifier'] == "" or self.config_parameters['applicationidentifier'] == {} or self.config_parameters['applicationidentifier'] == None):
            self.validate_application_identifier()
        self.validate_allowed_domain()
        self.validate_k2email()
        self.validate_k2password()
        self.validate_k2cloud()

        if self.config_parameters["isauthrequired"]:
            self.validate_application_login_url()
            self.validate_application_login_identifier()
        if self.config_parameters["remotemachinecredentials"] != "" and self.config_parameters["remotemachinecredentials"] != {} and self.config_parameters["remotemachinecredentials"] != None:
            self.validate_remote_machine_credentials()
        return self.config_parameters


class ConfigureJenkinsParameters(ConfigureCliParameters):
    """docstring for ConfigureJenkinsParameters."""

    def __init__(self, **kwargs):
        super(ConfigureJenkinsParameters, self).__init__(**kwargs)

    def validate_install_agent_parameters(self):
        self.validate_k2email()
        self.validate_k2password()
        if not (self.config_parameters['applicationidentifier'] == "" or self.config_parameters['applicationidentifier'] == {} or self.config_parameters['applicationidentifier'] == None):
            self.validate_application_identifier()
        if self.config_parameters["remotemachinecredentials"] != "" and self.config_parameters["remotemachinecredentials"] != {} and self.config_parameters["remotemachinecredentials"] != None:
            self.validate_remote_machine_credentials()
        self.validate_k2cloud()
        return self.config_parameters

    def validate_extract_result_parameters(self):
        self.validate_k2email()
        self.validate_k2password()
        self.validate_application_identifier()
        if self.config_parameters["remotemachinecredentials"] != "" and self.config_parameters["remotemachinecredentials"] != {} and self.config_parameters["remotemachinecredentials"] != None:
            self.validate_remote_machine_credentials()
        self.validate_k2cloud()
        return self.config_parameters

    def validate_crawl_application_parameters(self):
        self.validate_application_url()
        self.validate_ignore_url()
        self.validate_allowed_domain()

        if self.config_parameters["isauthrequired"]:
            self.validate_application_login_url()
            self.validate_application_login_identifier()
        if self.config_parameters["remotemachinecredentials"] != "" and self.config_parameters["remotemachinecredentials"] != {} and self.config_parameters["remotemachinecredentials"] != None:
            self.validate_remote_machine_credentials()
        return self.config_parameters


user_config = UserConfig()
project_config = ProjectConfig()