Sample Example
---------

- Let's suppose Your login page looks like this

<img src="static/Images/Login Page.png" alt="My cool logo"/>
&nbsp

- Collect unique id of input fields on login page like this

<img src="static/Images/email_field.png" alt="My cool logo"/>
&nbsp

<img src="static/Images/password_field.png" alt="My cool logo"/>
&nbsp

<img src="static/Images/submit_button.png" alt="My cool logo"/>
&nbsp

- Now using these unique ids for input field and submit button, prepare a JSON like this.
```sh
{"username": {"identification": "type=\"text\"", "value": "john@acme.inc"}, "password": {"identification": "type=\"password\"", "value": "123456"}, "submit": {"identification": "type=\"submit\"", "value": "Login"}}
```

- Now our final k2hackbot command will be
```sh
k2hackbot verify-application --applicationUrl "http://[ip]:[port]/app/login" -isauthreq true --applicationIdentifier "{\"pid\":23,\"containerid\":\"de63415703d8\"}" --applicationLoginIdentifier "{\"username\": {\"identification\": \"type=\\\"text\\\"\", \"value\": \"john@acme.inc\"}, \"password\": {\"identification\": \"type=\\\"password\\\"\", \"value\": \"123456\"}, \"submit\": {\"identification\": \"type=\\\"submit\\\"\", \"value\": \"Login\"}}"
```
