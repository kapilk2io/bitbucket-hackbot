from setuptools import setup

with open("requirements.txt") as file:
    install_requires = file.read().splitlines()
extras_require = {}

setup(
    name='k2hackbot',
    version='0.0.1',
    packages=['k2hackbot', 'k2hackbot.k2zap', 'k2hackbot.k2agent', 'k2hackbot.k2crawler', 'k2hackbot.k2crawler.k2crawler', 'k2hackbot.k2crawler.k2crawler.spiders', 'k2hackbot.k2manager', 'k2hackbot.global_classes', 'k2hackbot.custom_exceptions', 'k2hackbot.environment_validation', "k2hackbot.result_extractor", "k2hackbot.k2crawler.application_interaction", "k2hackbot.config", "k2hackbot.console_handler", "k2hackbot.k2logger", "k2hackbot.k2browser"],
    url='',
    license='',
    include_package_data = True,
    author='devanshsaini',
    author_email='devansh@k2io.com',
    description='K2ads is automated system for Vulnerability scanning',
    entry_points={
        'console_scripts': ['k2hackbot = k2hackbot.k2hackbot_jenkins:main']
    },
    python_requires='>=3.6',
    install_requires=install_requires,
    extras_require=extras_require,
    classifiers=[
        'Development Status :: 3 - Alpha',  # Chose either "3 - Alpha", "4 - Beta" or "5 - Production/Stable" as the current state of your package
        'Intended Audience :: Developers',  # Define that your audience are developers
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',  # Again, pick a license
        'Programming Language :: Python :: 3.7',  # Specify which pyhton versions that you want to support
    ],
)