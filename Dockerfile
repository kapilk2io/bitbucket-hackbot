FROM joyzoursky/python-chromedriver:latest

COPY k2hackbot /k2hackbot
COPY requirements.txt /



RUN pip3 install -r /requirements.txt

ENTRYPOINT ["python3","/k2hackbot/main.py"]