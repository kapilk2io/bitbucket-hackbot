Sample Example
---------

- Create a config file
- Provide options of k2hackbot command in JSON format
e.g.
```sh
{
  "isAuthRequired": "true",
  "applicationLoginUrl":"http://[ip]:[port]/app/login",
  "applicationloginidentifier":"{\"username\": {\"identification\": \"name=\\\"username\\\"\",\"value\": \"admin\"},\"password\": {\"identification\": \"name=\\\"password\\\"\",\"value\": \"admin123\"},\"submit\": {\"identification\": \"id=\\\"clickButton\\\"\",\"value\": \"Login\"}}",
  "applicationIdentifier": {"containerid":"application_container_id","pid":application_pid},
  "applicationurl": ["http://[ip]:[port]"],
  "ignoreurl": [],
  "k2email":"your_k2_email",
  "k2password": "your_k2_account_password",
  "remotemachinecredentials":""
}
```
- Now using this config file, run K2HackBot:
```sh
k2hackbot verify-application --config path_of_config_file
```
